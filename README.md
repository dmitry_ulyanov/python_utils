# Dependencies

- [forbiddenfruit](https://github.com/clarete/forbiddenfruit)
- [runipy](https://github.com/paulgb/runipy) (optional)

# Install 
Make sure you enabled macros in Jupyter. You need this line 
> c.StoreMagics.autorestore = True

to be present in ipython_config.py in your ipython profile.

Then run
> runipy Macro.ipynb

or open Jupyter and run Macro.ipynb there.