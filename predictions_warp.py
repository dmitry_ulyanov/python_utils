import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def ApplyPower(labels, predictions, loss, plot=False):

    ab = []
    for p in np.arange(-1, 5, 0.05):

        pred = predictions.copy()
        pred[predictions > 0] = np.power(predictions[predictions > 0], p)
       # pred = np.power(predictions,p)
        # print pred
        pred = pred / np.sum(pred, axis=1)[:, None]

        ab.append([p, loss(labels, pred[:, 1])])

    ab = pd.DataFrame(np.vstack(ab))
    if plot:
        plt.plot(ab.iloc[:, 1])
        plt.xticks(range(ab.shape[0])[::3], ab.ix[::3, 0].values)

    ab.sort(1, inplace=True)

    pred = np.power(predictions, (ab.iloc[0, 0]))
    pred = pred / np.sum(pred, axis=1)[:, None]

    return ab.iloc[0, 0], pred, loss(labels, pred[:, 1])


def ApplySigmoid(labels, predictions, loss, plot=False):

    ab = []
    for beta in np.arange(0, 3, 0.25):
        for alpha in np.arange(1, 3, 0.1):
            pred = beta * \
                (1. / (1 + np.exp(-(predictions - 0.5) * alpha)) - 0.5) + 0.5
            ab.append([beta, alpha, loss(labels, pred)])

    ab = pd.DataFrame(np.vstack(ab))
    if (plot):
        plt.plot(ab.iloc[:, 2])

    ab.sort(2, inplace=True)

    beta = ab.iloc[0, 0]
    alpha = ab.iloc[0, 1]

    pred = beta * (1. / (1 + np.exp(-(predictions - 0.5) * alpha)) - 0.5) + 0.5

    return (ab.iloc[0, 0], ab.iloc[0, 1]),  pred, loss(labels, pred)


'''

mix2(r1['y'],r1['pr'],r2['pr'],RMSE, np.arange(-0.5, 1.5, 0.1),'s',p=0.6)

a -- arithmetic
g -- geom
s -- minkowski slyle
'''


def mix2(y, pr1_in, pr2_in, loss, r=None, mode='a', p=2, minimize=True):
    # ============================

    def f_a(alpha, pr1, pr2):
        return np.maximum(pr1 * alpha + pr2 * (1 - alpha), 0)

    def f_g(alpha, pr1, pr2):
        return np.maximum((pr1 ** alpha) * (pr2**(1 - alpha)), 0)

    def f_s(alpha, pr1, pr2):
        return np.maximum((pr1**p) * alpha + (pr2**p) * (1 - alpha), 10e-15)**(1.0 / p)

    # ==============================

    if r is None:
        r = np.arange(0, 1, 0.05)

    isdict = False
    if type(pr1_in) == dict and type(pr1_in) == dict:
        pr1 = pr1_in['pr']
        pr2 = pr2_in['pr']
        isdict = True
    else:
        pr1 = pr1_in
        pr2 = pr2_in

    pr1_ = np.maximum(pr1, 10e-15)
    pr2_ = np.maximum(pr2, 10e-15)

    if mode == 'a':
        f = f_a
    elif mode == 'g':
        f = f_g
    elif mode == 's':
        f = f_s

    l = []
    for alpha in r:
        l.append(loss(y, f(alpha, pr1_, pr2_)))

    plt.plot(r, l)
    # plt.show()

    if not minimize:
        l = [-x for x in l]

    i = np.argmin(l)
    mix = f(r[i], pr1_, pr2_)

    print 'pr1 = %f\npr2= %f\nmix = %f, alpha = %f' % (loss(y, pr1), loss(y, pr2), min(l), r[np.argmin(l)])
    if not isdict:
        return mix
    else:
        pr3 = pr1_in.copy()
        pr3['pr'] = mix

        if 'pr_test' in pr1_in:
            pr1_test = np.maximum(pr1_in['pr_test'], 10e-15)
            pr2_test = np.maximum(pr2_in['pr_test'], 10e-15)

            pr3['pr_test'] = f(r[i], pr1_test, pr2_test)

        return pr3
            # print loss(y_test, pr_rf)


def mix21(y, pr1_in, pr2_in, loss, r=None, mode='a', p=2, minimize=True):
    # ============================

    def f_a(alpha, pr1, pr2):
        return pr1 * alpha + pr2 * (1 - alpha)

    def f_g(alpha, pr1, pr2):
        return (pr1 ** alpha) * (pr2**(1 - alpha))

    def f_s(alpha, pr1, pr2):
        return ((pr1**p) * alpha + (pr2**p) * (1 - alpha))**(1.0 / p)

    # ==============================

    if r is None:
        r = np.arange(0, 1, 0.05)

    isdict = False
    if type(pr1_in) == dict and type(pr1_in) == dict:
        pr1 = pr1_in['pr']
        pr2 = pr2_in['pr']
        isdict = True
    else:
        pr1 = pr1_in
        pr2 = pr2_in

    pr1_ = pr1
    pr2_ = pr2

    if mode == 'a':
        f = f_a
    elif mode == 'g':
        f = f_g
    elif mode == 's':
        f = f_s

    l = []
    for alpha in r:
        l.append(loss(y, f(alpha, pr1_, pr2_)))

    plt.plot(r, l)
    # plt.show()

    if not minimize:
        l = [-x for x in l]

    i = np.argmin(l)
    mix = f(r[i], pr1_, pr2_)

    print 'pr1 = %f\npr2= %f\nmix = %f, alpha = %f' % (loss(y, pr1), loss(y, pr2), min(l), r[np.argmin(l)])
    if not isdict:
        return mix
    else:
        pr3 = pr1_in.copy()
        pr3['pr'] = mix

        if 'pr_test' in pr1_in:
            pr1_test = pr1_in['pr_test']
            pr2_test = pr2_in['pr_test']

            pr3['pr_test'] = f(r[i], pr1_test, pr2_test)

        return pr3
            # print loss(y_test, pr_rf)
