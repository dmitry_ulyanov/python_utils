from sklearn.ensemble import RandomForestRegressor, ExtraTreesRegressor
from sklearn.cross_validation import KFold, StratifiedKFold

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import xgboost as xgb

from sklearn.metrics import mean_squared_error

import logging
from sklearn.neighbors import KNeighborsRegressor
from sklearn.svm import LinearSVR, SVR, NuSVR
from sklearn.preprocessing import StandardScaler

from util import ApplyPower, get_confusion_matrix, log_loss
from sklearn.feature_extraction.text import TfidfTransformer


from sklearn.linear_model import LinearRegression, ElasticNet
import scipy.sparse


class trylib_r:

    def init_dicts(self):

        # self_run_groups = {
        #     'rf' : 'rf',
        #     'et' : 'rf',

        #     'knn':

        #     'lsvc' : 'svm',
        #     'svm' : 'svm'

        #     'nusvc' : 'nusvc'
        # }

        self.run_dict = {
            'rf': self.apply_predict,
            'et': self.apply_predict,

            'knn': self.apply_predict,

            #'xg': self.apply_predict_proba,
            'lsvr': self.apply_predict,

            'svr': self.apply_predict,
            'nusvr':  self.apply_predict,

            'linreg': self.apply_predict,
            'en': self.apply_predict,

            'xg': self.apply_xgb,
            'fm': 'not yet'
        }

        self.clf_dict = {
            'rf': RandomForestRegressor(n_jobs=6),
            'et': ExtraTreesRegressor(n_jobs=6),

            'lsvr': LinearSVR(),
            'svr': SVR(),
            'nusvr': NuSVR(),
            'xg': 'xg',

            'en': ElasticNet(),
            'knn': KNeighborsRegressor(),
            'linreg': LinearRegression()
        }

        self.preprocess_dict = {
            'lsvr': self.preprocess_lsvc,
            'nusvr': self.preprocess_nusvc,

            #'knn' : self.preprocess_lsvc,
            #'rf' : self.preprocess_rf,
            #'xg' : self.preprocess_lsvc,

        }

        self.postprocess_dict = {
            #'lsvc': self.postprocess_lsvc,
            #'nusvc': self.postprocess_lsvc,

            'rf': self.postprocess_rf,
            'et': self.postprocess_rf,
            #'knn' : self.postprocess_rf,
        }

    def __init__(self, X, Y, clf_str, params, skf=10, one=True, fold_num=0, skf_seed=11, X_test=None, test_mode=''):
        self.clf_str = clf_str
        self.params = params
        self.one = one
        self.skf_seed = skf_seed
        self.fold_num = fold_num
        self.predict_test = X_test is not None
        self.skf = self.get_skf(Y, skf)
        self.test_mode = test_mode

        self.init_dicts()
        print 1
        self.res = self.try_clf(X, Y, X_test)

    def try_clf(self, X, Y, X_test):
        clf_str = self.clf_str
        params = self.params

        if not self.one:
            pr_train = np.zeros(X.shape[0])

        if self.predict_test:
            pr_test = np.zeros(X_test.shape[0])
            count = 0

        f = self.run_dict[clf_str]

        if self.test_mode == 'whole':
            r_t = f(X, Y, X, Y, X_test)
            pr_test = r_t['pr_test']
            return {'pr_test': pr_test}

        for it, (train_index, val_index) in enumerate(self.skf):

            if self.one and self.fold_num != it:
                continue

            X_train, X_val = X[train_index], X[val_index]
            y_train, y_val = Y[train_index], Y[val_index]

            if clf_str in self.preprocess_dict:
                X_train, X_val, X_test = self.preprocess_dict[
                    clf_str](X_train,  X_val,  X_test)

            r = f(X_train, y_train, X_val, y_val, X_test)

            if clf_str in self.postprocess_dict:
                post_f = self.postprocess_dict[clf_str]
                post_f(r)

            r['train_index'] = train_index
            r['val_index'] = val_index

            if self.predict_test:
                pr_test += r['pr_test']
                count += 1

            if self.one:
                print 1
                #get_confusion_matrix(r['pr_val'], r['y_val'])
                return r
            else:
                print '%d fold loss = %f' % (it, r['loss'])
                pr_train[val_index] = r['pr']

        loss = np.sqrt(mean_squared_error(Y, pr_train))
        d_out = {
            'pr': pr_train,
            'loss': loss,
            'y': Y
        }

        if self.predict_test:
            d_out['pr_test'] = pr_test / count

            if self.test_mode == 'whole':
                r_t = f(X, Y, X, Y, X_test)
                d_out['pr_test'] = r_t['pr_test']

        return d_out

    def apply_predict(self, X_train, y_train, X_val, y_val, X_test):

        pr_test = None

        clf = self.clf_dict[self.clf_str]

        clf.set_params(**self.params)

        clf.fit(X_train, y_train)

        pr_val = clf.predict(X_val)
        loss_val = np.sqrt(mean_squared_error(y_val, pr_val))

        if self.predict_test:
            pr_test = clf.predict(X_test)

        return {
            'clf': clf,
            'pr': pr_val,
            'y': y_val,
            'loss': loss_val,
            'pr_test': pr_test  # None if  X_test = none
        }

    def get_skf(self, Y, n_folds):
        return StratifiedKFold(Y, n_folds=n_folds, shuffle=True, random_state=self.skf_seed)

    # def get_skf(self, Y, n_folds):
    #     return KFold(Y.shape[0], n_folds=n_folds, shuffle=True, random_state=self.skf_seed)

    # def apply_xgb(self, X_train, y_train, X_val, y_val, X_test):
    #     pr_test = None

    #     dtrain = xgb.DMatrix(X_train, label=y_train)
    #     dval = xgb.DMatrix(X_val, label=y_val)

    #     watchlist = [(dval, 'eval'), (dtrain, 'train')]

    #     bst = xgb.train(
    #         self.params, dtrain, self.params['num_round'], watchlist)

    #     pr_val = bst.predict(dval)
    #     loss_val = np.sqrt(mean_squared_error(y_val, pr_val))

    #     if self.predict_test:
    #         dtest = xgb.DMatrix(X_test)
    #         pr_test = bst.predict(dtest)

    #     return {
    #         'clf': bst,
    #         'pr': pr_val,
    #         'y': y_val,
    #         'loss': loss_val,
    #         'pr_test': pr_test  # None if  X_test = none
    #     }

    def apply_xgb(self, X_train, y_train, X_val, y_val, X_test):
        pr_test = None

        dtrain = xgb.DMatrix(X_train, label=y_train)
        dval = xgb.DMatrix(X_val, label=y_val)
        
        dtrain.set_base_margin(y_train*0)
        dval.set_base_margin(y_val*0)
            
        watchlist = [(dtrain, 'train'),(dval, 'eval')]

#         bst = xgb.train(
#             params, dtrain, params['num_round'], watchlist)
        # feval = None
        # if 'feval' in self.params:
        #     self.params['None']

        if 'early_stopping_rounds' in self.params:
            bst = xgb.train(
                self.params, dtrain, self.params['num_round'], watchlist,early_stopping_rounds = self.params['early_stopping_rounds'])
            print 'bi: ', bst.best_iteration
            pr_val = bst.predict(dval,ntree_limit=bst.best_iteration)
        else:
            bst = xgb.train(
                self.params, dtrain, self.params['num_round'], watchlist, feval = self.params.get('feval'), obj = self.params.get('obj'))

            pr_val = bst.predict(dval)
        
        #pr_val = bst.predict(dval)
        loss_val = np.sqrt(mean_squared_error(y_val, pr_val))

        if self.predict_test:
            dtest = xgb.DMatrix(X_test)
            pr_test = bst.predict(dtest)

        return {
            'clf': bst,
            'pr': pr_val,
            'y': y_val,
            'loss': loss_val,
            'pr_test': pr_test  # None if  X_test = none
        }

    # ========+ preprocess
    def preprocess_rf(self, X_train, X_val, X_test):
        # return

#         if self.predict_test:
#             train_test = np.concatenate([X_train, X_val, X_test], axis=0)
#         else:
#             train_test = np.concatenate([X_train, X_val], axis=0)

#         sc = StandardScaler()
#         sc.fit(train_test)

#         tfidf = TfidfTransformer()
#         tfidf.fit(train_test)

#         X_train = sc.transform(X_train)
# X_train = tfidf.transform(X_train)

#         X_val = sc.transform(X_val)
# X_val = tfidf.transform(X_val)

#         if self.predict_test:
#             X_test = sc.transform(X_test)
# X_test = tfidf.transform(X_test)

        return X_train, X_val, X_test

    def preprocess_nusvc(self, X_train, X_val, X_test):
        '''
        tf-idf makes it worse
        scaling helps in conv speed and accuracy
        '''

        if self.predict_test:
            if scipy.sparse.issparse(X_train):
                train_test = scipy.sparse.vstack(
                    [X_train, X_val, X_test], format='csr')
            else:
                train_test = np.concatenate([X_train, X_val, X_test], axis=0)
        else:
            if scipy.sparse.issparse(X_train):
                train_test = scipy.sparse.vstack(
                    [X_train, X_val], format='csr')
            else:
                train_test = np.concatenate([X_train, X_val], axis=0)

        # sc = StandardScaler()
        # sc.fit(train_test)

        # X_train = sc.transform(X_train)

        # X_val = sc.transform(X_val)

        # if self.predict_test:
        #     X_test = sc.transform(X_test)

        return X_train, X_val, X_test

    def preprocess_lsvc(self, X_train, X_val, X_test):
        # return

        # if self.predict_test:
        #     if scipy.sparse.issparse(X_train):
        #         train_test = scipy.sparse.vstack(
        #             [X_train, X_val, X_test], format='csr')
        #     else:
        #         train_test = np.concatenate([X_train, X_val, X_test], axis=0)
        # else:
        #     if scipy.sparse.issparse(X_train):
        #         train_test = scipy.sparse.vstack(
        #             [X_train, X_val], format='csr')
        #     else:
        #         train_test = np.concatenate([X_train, X_val], axis=0)

        # sc = StandardScaler()
        # sc.fit(train_test)

        # tfidf = TfidfTransformer()
        # tfidf.fit(train_test)

        # X_train = sc.transform(X_train)
        # X_train = tfidf.transform(X_train)

        # X_val = sc.transform(X_val)
        # X_val = tfidf.transform(X_val)

        # if self.predict_test:
        #     X_test = sc.transform(X_test)
        #     X_test = tfidf.transform(X_test)

        return X_train, X_val, X_test
        #X_train = sc.fit_transform(X)
        #X_test =  sc.transform(X_test)

        #X = TfidfTransformer().fit_transform(Xs)
        #X_test =  sc.transform(X_test)
        # pass

    # ========= postprocess
    def postprocess_rf(self, r):
        pass
#         power, _, loss = ApplyPower(
#             r['y'], r['pr'], mean_squared_error, plot=False)
#         print 'mean_squared_error = ', np.sqrt(loss), ' power = ', power

    def postprocess_lsvc(self, r):
        pass
