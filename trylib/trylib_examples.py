
# coding: utf-8

# # Trylib example

# ## RF

# In[ ]:

param = {'n_estimators': 100,
             'criterion' : 'gini',
             'max_depth':None,
             'max_features' : 'auto',
             'min_samples_leaf' : 1,
         
             'n_jobs':-1,
}

res = trylib(X,Y,'rf',param,one = False,skf_seed = 12, skf = 10, X_test= X_test).res


# # ET

# In[ ]:

param = {'n_estimators': 100,
         'criterion' : 'gini',
         'max_depth':None,
         'max_features' : 'auto',
         'min_samples_leaf' : 1,
         
         'n_jobs':-1,
}

res = trylib(X,Y,'et',param,one = False,skf_seed = 12, skf = 10, X_test= X_test).res


# ## XGBoost

# In[ ]:

get_ipython().run_cell_magic(u'capture', u'--no-stdout --no-display', u"param = {'num_round': 200,\n         \n             'seed' : 2441,\n             'max_depth':4,\n             'gamma': 0,\n             'eta':0.02,\n             'min_child_weight':1,\n             'silent':1, \n\n             'objective':'binary:logistic',\n             'scale_pos_weight':3,\n\n             'subsample' : 0.95,\n             'colsample_bytree' : 0.2,\n             #'base_score':Y.mean()\n           }\n\n# Objectives:\n\n#  -- 'objective':'binary:logistic',\n#  -- 'objective':'multi:softprob',\n#  -- 'objective':'rank:pairwise',\n#  -- 'objective':'reg:linear',\n\n# eval_metrics:\n\n#  -- 'eval_metric':'mlogloss',\n#  -- 'eval_metric':'auc',\n\n# num_class:\n\n#  -- 'num_class':9\n\nres = trylib(X,Y,'xg',param,one = False,skf_seed = 12, skf = 10, X_test= X_test).res")


# ## Linear SVC

# In[ ]:

param = {'C':1.0,
         'penalty':'l2',
         'loss':'squared_hinge',
         'dual':True,
         'multi_class':'ovr',
         'class_weight': None,
         'random_state':329,
        }

res = trylib(X,Y,'lsvc',param,one = False,skf_seed = 12, skf = 10, X_test= X_test).res


# ## KNN

# In[ ]:

param = {'n_neighbors':5,
         'weights':'uniform',
         'metric':'minkowski',
        }

# weights:

# -- 'weights':'uniform'
# -- 'weights':'distance'
# [callable]

# metrics:

# -- 'metric':'euclidean'
# -- 'metric':'cityblock'
# -- 'metric':'canberra'
# -- 'metric':'braycurtis'
# cosine ? 

res = trylib(X,Y,'knn',param,one = False,skf_seed = 12, skf = 10, X_test= X_test).res


# # Holdout validation

# In[ ]:

get_ipython().run_cell_magic(u'capture', u'--no-stdout --no-display', u"from sklearn.cross_validation import train_test_split\n\nfor i in range(4):\n    x_train, x_ho, y_train, y_ho = train_test_split(X, Y, \n                                                test_size=0.25, \n                                                random_state=i)\n#     x_train  = x_train.values\n#     x_hohout = x_hohout.values\n#     y_train  = y_train.values\n#     y_ho     = y_ho.values\n    \n    param = { }\n    \n    \n    res = trylib_r(x_train,y_train,'rf',param,one = True,skf = 5,X_test=x_ho,skf_seed = 12).res\n\n    print 'cv ',res['loss']\n    print 'ho ',RMSE(y_ho,res['pr_test'])    \n    \n    #print 'cv classes ', res['accuracy']\n    #print 'ho classes ', accuracy_score(y_ho,np.argmax(res['pr_test'],axis=1)) ")


# # 2 level

# ### 1

# In[ ]:

get_ipython().run_cell_magic(u'capture', u'--no-stdout --no-display', u'\n# X = \n# Y = \n# X_test =\n\n# ============ \ny_metas = []\ny_tests = []\n\nxg_param = {\'num_round\': 50,\n             \'seed\' : 21,\n             \'max_depth\':3,\n             \'gamma\': 0.01,\n             \'eta\':0.3,\n             \'min_child_weight\':1,\n             \'silent\':1, \n             \'objective\':\'multi:softprob\',\n             \'num_round\':100,\n             \'num_class\' : #9,\n             \'subsample\' : 0.8,\n             \'colsample_bytree\' : 0.2}\n\nclfs = [\n             (\'xg\', xg_param),\n             (\'lsvc\', {\'C\' : 100}),\n             (\'knn\', {\'n_neighbors\' : 10 , \'metric\': \'canberra\'}),\n             (\'knn\', {\'n_neighbors\' : 1 , \'metric\': \'canberra\'}),\n             (\'nusvc\',{\'nu\' : 0.007}),\n             (\'rf\',{\'max_features\' : 0.01,\'n_estimators\' : 4000}),\n             (\'rf\',{\'max_features\' : 0.3,\'n_estimators\' : 4000}),\n             (\'et\',{\'max_features\' : 0.1,\'n_estimators\' : 4000}),\n    ]\n\nfor c in clfs:\n    print \' ==== \', c[0], \' =====\'\n        \n    res = trylib(X,Y,c[0],c[1],skf =10, X_test = X_test).res\n    print " - %s: log_loss: %f, acc: %f" %(c[0], res[\'loss\'],res[\'accuracy\'])\n    y_metas.append(res[\'pr\'])\n    y_tests.append(res[\'pr_test\'])\n\ny_meta = res[\'y\']')


# ### 2

# In[ ]:

get_ipython().run_cell_magic(u'capture', u'--no-stdout --no-display', u'\nxg_param = {\n         \'seed\' : 21,\n         \'max_depth\':3,\n         \'gamma\': 0.01,\n         \'eta\':0.1,\n         \'silent\':1, \n         \'objective\':\'multi:softprob\',\n         \'num_round\':200,\n         \'num_class\' : 9,\n         \'subsample\' : 0.8,\n         \'colsample_bytree\' : 0.1}\nr = None\n\nmetas = np.hstack(y_metas)\ntest_metas = np.hstack(y_tests)\n\ny_out = []\nfor i in range(10):\n    xg_param[\'seed\'] = (i+34)*15\n    \n    res = trylib(metas,y_meta,\'xg\',xg_param,skf = 10,one = False, X_test= test_metas).res\n    y_out.append(res[\'pr_test\'])\n    \n    if r is None:\n        r = res[\'pr\']\n    else:\n        r += res[\'pr\']\n    print "%d %f" % (i, log_loss(y_val,r/(i+1)))\n\npr_test_final = np.mean(y_out,axis = 0)\npr_test_final.shape\n    \n#res2 = trylib(np.concatenate([d.values[res[\'val_index\'],:],np.hstack(prs)],axis = 1),y_val,\'et\',{\'max_features\' : 0.1,\'n_estimators\' : 20000, \'compute_importances\': True},skf = 10,one = False).res\n\n#prs.append(res[\'pr_val\'])\n#y_val = res[\'y_val\']')


# # CV template

# In[ ]:

skf = KFold(Y.shape[0], n_folds=10, shuffle=True, random_state=11)

for it, (train_index, test_index) in enumerate(skf):
    X_train, X_val = X[train_index], X[test_index]
    y_train, y_val = Y[train_index], Y[test_index]

