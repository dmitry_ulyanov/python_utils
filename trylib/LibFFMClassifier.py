from __future__ import absolute_import
import os
import sys
import subprocess
import shlex
import tempfile
import time
import sklearn.base
import math
import numpy as np
import pandas as pd
import scipy
from ExeEstimator import ExeEstimator
import timeit

_default_path = '/home/dmitry/git/libffm'


class LibFFMClassifier(ExeEstimator, sklearn.base.ClassifierMixin):

    '''
      options:
      -l <lambda>: set regularization parameter (default 0)
      -k <factor>: set number of latent factors (default 4)
      -t <iteration>: set number of iterations (default 15)
      -r <eta>: set learning rate (default 0.1)
      -s <nr_threads>: set number of threads (default 1)
      -p <path>: set path to the validation set
      --quiet: quiet model (no output)
      --norm: do instance-wise normalization
      --no-rand: disable random update
      `--norm' helps you to do instance-wise normalization. When it is enabled,
      you can simply assign `1' to `value' in the data.
    '''

    def __init__(self, lambda_v=0, factor=4, n_iter=15, eta=0.1,
                 n_jobs=1, quiet=False, normalize=None, no_rand=None, field=None):
        ExeEstimator.__init__(self)

        self.lambda_v = lambda_v
        self.factor = factor
        self.n_iter = n_iter
        self.eta = eta
        self.n_jobs = n_jobs
        self.quiet = quiet
        self.normalize = normalize
        self.no_rand = no_rand

        self.model_file = None
        self.field = field

        self.last_train_path = None

    def fit(self, X, y=None):
        if type(X) is str:
            train_path = X
        else:
            start_time = timeit.default_timer()
            train_path = self.tmpfile('_libffm_train')
            self.to_libffm(X, train_path, y)
            print('%f s for writing the file' % (timeit.default_timer() - start_time))

        start_time = timeit.default_timer()
        self.model_file = self.tmpfile('_libffm_model')
        #self.model_file = self.save_tmp_file(X, '_libffm_model', True)

        command = os.path.join(_default_path, 'ffm-train') + \
            ' -l ' + str(self.lambda_v) + \
            ' -k ' + str(self.factor) +  \
            ' -t ' + str(self.n_iter) + \
            ' -r ' + str(self.eta) + \
            ' -s ' + str(self.n_jobs)# + \
            #' -v ' + str(10)
        if self.quiet:
            command += ' --quiet'
        if self.normalize:
            command += ' --norm'
        if self.no_rand:
            command += ' --no-rand'
        command += ' ' + train_path
        command += ' ' + self.model_file
        print command
        running_process = self.make_subprocess(command)
        self.close_process(running_process)
        print('%f s for learning' % (timeit.default_timer() - start_time))
        return self

    def predict(self, X):
        test_file = self.tmpfile('_libffm_test')
        output_file = self.tmpfile('_libffm_predictions')
        self.to_libffm(X, test_file)

        command = os.path.join(_default_path, 'ffm-predict') + ' ' +      test_file + ' ' + self.model_file + ' ' + output_file
        running_process = self.make_subprocess(command)
        self.close_process(running_process)
        preds = list(self.read_predictions(output_file))
        return preds

    def predict_proba(self, X):
        print self.predict(X)
        predictions = np.asarray(
            map(lambda p: 1 / (1 + math.exp(-p)), self.predict(X)))
        return np.vstack([1 - predictions, predictions]).T

    def to_libffm(self, X, store_path, y=None):
        #join_time = 0
        def transform_row(row, y):
            #global join_time
            ffm = []

            if self.field is None:
                field = range(row.shape[1])
            else:
                field = self.field

            ffm.append(str(y))
            if scipy.sparse.issparse(row):
                nz = row.nonzero()
                # print nz
                for idx in xrange(len(nz[0])):
                    _, j = nz[0][idx], nz[1][idx]
                    ffm.append('{}:{}:{}'.format(0, j, row[0, j]))
            else:
                for i in xrange(row.shape[0]):
                    if row[i] != 0:
                        ffm.append('{}:{}:{}'.format(field[i], i, row[i]))

            start_time = timeit.default_timer()
            string = ' '.join(ffm)
            #print join_time
            #join_time = join_time + timeit.default_timer() - start_time
            return string

        if type(X) is pd.DataFrame:
            X = X.values
        if y is None:
            y = np.zeros(X.shape[0])
            print y.shape
        # print store_path
        f = open(store_path, 'w')
        # print X
        for i in xrange(X.shape[0]):
            row_str = transform_row(X[i, :], y[i])
            f.write("%s\n" % row_str)
        f.close()
        # print ('str join %f' % ())
