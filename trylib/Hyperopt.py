
# coding: utf-8

# In[ ]:

def apply_xgb(params, X_train, y_train, X_val, y_val):

        dtrain = xgb.DMatrix(X_train, label=y_train)
        watchlist = [(dtrain, 'train')]
        dval = xgb.DMatrix(X_val, label=y_val)
        
        if y_val is not None:
            watchlist.append((dval, 'eval'))
    
#         params['seed'] += np.random.randint(1000)
        
        if 'early_stopping_rounds' in params:
            bst = xgb.train(
                params, dtrain, params['num_round'], watchlist,early_stopping_rounds = params['early_stopping_rounds'])
            print 'bi: ', bst.best_iteration
            pr_val = bst.predict(dval,ntree_limit=bst.best_iteration)
        else:
            bst = xgb.train(
                params, dtrain, params['num_round'], watchlist)

            pr_val = bst.predict(dval)

        if len(pr_val.shape) == 1:
            pr_val = np.concatenate(
                [1 - pr_val[:, None], pr_val[:, None]], axis=1)
        
        if y_val is not None:
            loss_val = log_loss(y_val, pr_val)
            accuracy_val = accuracy_score(y_val, np.argmax(pr_val, axis=1))
    #         auc = AUC(y_val, pr_val[:, 1])
        else:
            loss_val = -1
            accuracy_val = -1
      

        return {
            'clf': bst,
            'pr_test': pr_val,
            'y': y_val,
            'loss': loss_val,
            'accuracy': accuracy_val,
#             'auc': auc
        }


# In[ ]:

get_ipython().run_cell_magic(u'capture', u'--no-stdout --no-display', u"from hyperopt import fmin, tpe\ndef xgb_score(param):\n    res = trylib_r(train.values,Y.values,'xg',param,one = True,skf_seed = 12, skf = 10, X_test= test.values).res\n    #print res\n    score = res['loss']\n    \n    print param\n    print score\n    print '==========='\n    trials.append([score,param])\n    \n    return score\n    \ndef xgb_hopt():\n    from hyperopt import hp\n    space = {\n         'eta' : 0.01,#hp.quniform('eta', 0.01, 0.1, 0.005),\n         'max_depth' : hp.quniform('max_depth', 10, 30,1),\n         'min_child_weight' : hp.quniform('min_child_weight', 0, 100, 1),\n         'subsample' : hp.quniform('subsample', 0.1, 1.0, 0.1),\n         'gamma' : hp.quniform('gamma', 0.0, 30, 0.5),\n         'colsample_bytree' : hp.quniform('colsample_bytree', 0.1, 1.0, 0.1),\n    \n         'objective':'reg:linear',\n\n         'nthread' : 28,\n         'silent' : 1,\n         'num_round' : 2500,\n         'seed' : 2441,\n         'early_stopping_rounds':100\n         }\n    \n    \n    best = fmin(xgb_score, space, algo=tpe.suggest, max_evals=1000)")


# In[ ]:

get_ipython().run_cell_magic(u'capture', u'--no-stdout --no-display', u'trials = []\nxgb_hopt()')


# In[ ]:

import sys, os

class Logger(object):
    "Lumberjack class - duplicates sys.stdout to a log file and it's okay"
    #source: http://stackoverflow.com/q/616645
    def __init__(self, filename="Red.Wood", mode="w", buff=0):
        self.stdout = sys.stdout
        self.file = open(filename, mode, buff)
        sys.stdout = self

    def __del__(self):
        self.close()

    def __enter__(self):
        pass

    def __exit__(self, *args):
        pass

    def write(self, message):
        self.stdout.write(message)
        self.file.write(message)

    def flush(self):
        self.stdout.flush()
        self.file.flush()
        os.fsync(self.file.fileno())

    def close(self):
        if self.stdout != None:
            sys.stdout = self.stdout
            self.stdout = None

        if self.file != None:
            self.file.close()
            self.file = None
Log=Logger('log1.log')

