from sklearn.ensemble import RandomForestRegressor
from sklearn.cross_validation import StratifiedKFold
import matplotlib.pyplot as plt
import numpy as np
import sys
sys.path.append('/home/dmitry/git/xgboost/wrapper')
import xgboost as xgb

from sklearn.cross_validation import KFold
from sklearn.metrics import mean_squared_error
from sklearn.metrics import confusion_matrix
import hickle
import logging

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


def get_skf(Y):
    return KFold(Y.shape[0], n_folds=2, shuffle=True, random_state=11)


def try_rf(X, Y, depth, n_estim, one=False, X_test=None):
    skf = get_skf(Y)

    if X_test is None:
        test = False
    else:
        test = True

    clf = RandomForestRegressor(max_depth=depth,
        n_estimators=n_estim, n_jobs=6)

    if not one:
        preds_all = np.zeros(X.shape[0])

    if test:
        preds_test = np.zeros(X_test.shape[0])
        count = 0


    for it, (train_index, test_index) in enumerate(skf):

        X_train, X_val = X[train_index], X[test_index]
        y_train, y_val = Y[train_index], Y[test_index]

        clf.fit(X_train, y_train)

        pr_val = clf.predict(X_val)

        if test:
            preds_test += clf.predict(X_test)
            count += 1

        loss = np.sqrt(mean_squared_error(y_val, pr_val))

        print loss

        if one:
            plt.figure(figsize=(12, 8))
            plt.grid(True, which="both")
            plt.plot(clf.feature_importances_)
            plt.show()

            return pr_val, y_val, clf.feature_importances_#, preds_test / count

        else:
            preds_all[test_index] = pr_val

    if test:
        return preds_all, preds_test / count

    return preds_all


def try_xg(X, Y, param, num_round=50, one=False, X_test=None,N=1):
#    N = 1

    if X_test is None:
        test = False
    else:
        test = True
        dtest = xgb.DMatrix(X_test)

    skf = get_skf(Y)

    preds_all = np.zeros(X.shape[0])

    if test:
        preds_test = np.zeros(X_test.shape[0])
        count = 0

    for it, (train_index, test_index) in enumerate(skf):

        #print train_index
        # print test_index
        X_train, X_val = X[train_index], X[test_index]
        y_train, y_val = Y[train_index], Y[test_index]

        dtrain = xgb.DMatrix(X_train, label=y_train)
        dval = xgb.DMatrix(X_val, label=y_val)

        watchlist = [(dval, 'eval'), (dtrain, 'train')]

        # ================== XGB
        # sc = np.zeros([len(test_index), 9])

        for i in range(N):
            param['seed'] = 21 + i

            bst = xgb.train(param, dtrain, num_round, watchlist)

            yprob = bst.predict(dval)

            if test:
                preds_test += bst.predict(dtest)
                count += 1

            logging.debug("%d %f" % (i, np.sqrt(mean_squared_error(y_val, yprob))))

            preds_all[test_index] += yprob

        preds_all[test_index] /= N
            #

            # print ,'\n'

        if(one):
            get_confusion_matrix(yprob, y_val)
            return yprob, preds_test / count

    if test:
        return preds_all,preds_test / count
    return preds_all


def get_confusion_matrix(data, y):

    def plot_confusion_matrix(cm, title='Confusion matrix', cmap=plt.cm.Blues):
        plt.imshow(cm, interpolation='nearest', cmap=cmap)
        plt.title(title)
        plt.colorbar()

        plt.ylabel('True label')
        plt.xlabel('Predicted label')

    cm = confusion_matrix(
        y, np.argmax(data, axis=1), labels=np.arange(9))
    plt.rcParams['figure.figsize'] = (16, 16)

    cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    print('Normalized confusion matrix')
    print(cm)
    #plt.figure(figsize = [10,8])
    #plot_confusion_matrix(cm_normalized, title='Normalized confusion matrix')
    return cm, cm_normalized


def mix2(pr_xg, pr_rf, y_test):
    l = []
    for alpha in np.arange(-0.5, 1.5, 0.1):
        l.append(log_loss(y_test, pr_xg * alpha + (1 - alpha) * pr_rf))
    plt.plot(np.arange(-0.5, 1.5, 0.1), l)
    plt.show()
    print log_loss(y_test, pr_xg)
    print log_loss(y_test, pr_rf)
    print np.min(l)
