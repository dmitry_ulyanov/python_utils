def apply_xgb(params, X_train, y_train, X_val, y_val):

        dtrain = xgb.DMatrix(X_train, label=y_train)
        watchlist = [(dtrain, 'train')]
        dval = xgb.DMatrix(X_val, label=y_val)
        
        if y_val is not None:
            watchlist.append((dval, 'eval'))
    
#         params['seed'] += np.random.randint(1000)
        
        # if 'early_stopping_rounds' in params:
        bst = xgb.train(
            params, dtrain, params['num_round'], watchlist,
            early_stopping_rounds = params.get('early_stopping_rounds'),
            feval = self.params.get('feval'), 
            obj = self.params.get('obj')
            )
        print 'best_iter: ', bst.best_iteration
        pr_val = bst.predict(dval,ntree_limit=bst.best_iteration)
        
        # else:
        #     bst = xgb.train(
        #         params, dtrain, params['num_round'], watchlist)

        #     pr_val = bst.predict(dval)

        #   if 'early_stopping_rounds' in self.params:
        #     bst = xgb.train(
        #         self.params, dtrain, self.params['num_round'], watchlist,early_stopping_rounds = self.params['early_stopping_rounds'])
        #     print 'bi: ', bst.best_iteration
        #     pr_val = bst.predict(dval,ntree_limit=bst.best_iteration)
        # else:
        #     bst = xgb.train(
        #         self.params, dtrain, self.params['num_round'], watchlist, feval = self.params.get('feval'), obj = self.params.get('obj'))

        #     pr_val = bst.predict(dval)

        if len(pr_val.shape) == 1:
            pr_val = np.concatenate(
                [1 - pr_val[:, None], pr_val[:, None]], axis=1)
        
        if y_val is not None:
            loss_val = log_loss(y_val, pr_val)
            accuracy_val = accuracy_score(y_val, np.argmax(pr_val, axis=1))
    #         auc = AUC(y_val, pr_val[:, 1])
        else:
            loss_val = -1
            accuracy_val = -1
      

        return {
            'clf': bst,
            'pr_test': pr_val,
            'y': y_val,
            'loss': loss_val,
            'accuracy': accuracy_val,
#             'auc': auc
        }