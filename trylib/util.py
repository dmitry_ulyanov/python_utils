import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.metrics import confusion_matrix

#from sklearn.metrics import log_loss, accuracy_score, confusion_matrix


def ApplyPower(labels, predictions, loss, plot=False):

    ab = []
    for p in np.arange(-1, 5, 0.05):

        pred = predictions.copy()
        pred[predictions > 0] = np.power(predictions[predictions > 0], p)
       # pred = np.power(predictions,p)
        # print pred
        pred = pred / np.sum(pred, axis=1)[:, None]

        ab.append([p, loss(labels, pred)])

    ab = pd.DataFrame(np.vstack(ab))
    if plot:
        plt.plot(ab.iloc[:, 1])
        plt.xticks(range(ab.shape[0])[::3], ab.ix[::3, 0].values)

    ab.sort(1, inplace=True)

    pred = np.power(predictions, (ab.iloc[0, 0]))
    pred = pred / np.sum(pred, axis=1)[:, None]

    return ab.iloc[0, 0], pred, loss(labels, pred)


def ApplySigmoid(labels, predictions, loss, plot=False):

    ab = []
    for beta in np.arange(0, 3, 0.25):
        for alpha in np.arange(1, 3, 0.1):
            pred = beta * \
                (1. / (1 + np.exp(-(predictions - 0.5) * alpha)) - 0.5) + 0.5
            ab.append([beta, alpha, loss(labels, pred)])

    ab = pd.DataFrame(np.vstack(ab))
    if (plot):
        plt.plot(ab.iloc[:, 2])

    ab.sort(2, inplace=True)

    beta = ab.iloc[0, 0]
    alpha = ab.iloc[0, 1]

    pred = beta * (1. / (1 + np.exp(-(predictions - 0.5) * alpha)) - 0.5) + 0.5

    return (ab.iloc[0, 0], ab.iloc[0, 1]),  pred, loss(labels, pred)


def rand_jitter(arr):
    stdev = (arr.max() - arr.min()) / 200.
    return arr + np.random.randn(len(arr)) * stdev


def scj(x, y, s=20, c='b', marker='o', cmap=None, norm=None, vmin=None, vmax=None, alpha=None, linewidths=None, verts=None, hold=None, **kwargs):
    return plt.scatter(rand_jitter(x), rand_jitter(y), s, c, marker,  cmap, norm, vmin, vmax, alpha, linewidths, verts, hold, edgecolor='none', **kwargs)

def get_confusion_matrix(data, y):

    def plot_confusion_matrix(cm, title='Confusion matrix', cmap=plt.cm.Blues):
        plt.imshow(cm, interpolation='nearest', cmap=cmap)
        plt.title(title)
        plt.colorbar()

        plt.ylabel('True label')
        plt.xlabel('Predicted label')

    cm = confusion_matrix(
        y, np.argmax(data, axis=1), labels=np.arange(9))
    plt.rcParams['figure.figsize'] = (16, 16)

    cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    print('Normalized confusion matrix')
    print(cm)
    #plt.figure(figsize = [10,8])
    #plot_confusion_matrix(cm_normalized, title='Normalized confusion matrix')
    return cm, cm_normalized


def log_loss(y, y_pr):

    y = np.array(y)
    y_pr = np.array(y_pr)

    pred = y_pr[range(y.shape[0]), y]

    epsilon = 1e-15
    pred = np.maximum(epsilon, pred)
    pred = np.minimum(1 - epsilon, pred)

    ll = np.sum(np.log(pred))
    ll = ll * -1.0 / len(y)
    return ll
