from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier
from sklearn.cross_validation import StratifiedKFold

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import xgboost as xgb

from sklearn.metrics import accuracy_score, confusion_matrix

#import hickle
#import logging
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import LinearSVC, SVC, NuSVC, SVR
from sklearn.preprocessing import StandardScaler
from util import ApplyPower, get_confusion_matrix, log_loss
from sklearn.feature_extraction.text import TfidfTransformer

import os.path
import sys
sys.path.append(
    os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))
from alias import *


from sklearn.linear_model import LogisticRegression, SGDClassifier, RidgeClassifier
import scipy.sparse
import apply_f

class trylib:

    def init_dicts(self):

        # self_run_groups = {
        #     'rf' : 'rf',
        #     'et' : 'rf',

        #     'knn':

        #     'lsvc' : 'svm',
        #     'svm' : 'svm'

        #     'nusvc' : 'nusvc'
        # }

        self.run_dict = {
            'rf': self.apply_predict_proba,
            'et': self.apply_predict_proba,

            'knn': self.apply_predict_proba,

            #'xg': self.apply_predict_proba,
            'lsvc': self.apply_decision_function,

            'svc': self.apply_predict_proba,
            'nusvc':  self.apply_predict_proba,

            'logreg': self.apply_predict_proba,
            'ridge': self.apply_decision_function,
            'xg': self.apply_xgb,
            'fm': 'not yet',
            'sgd': self.apply_decision_function,
        }

        self.clf_dict = {
            'rf': RandomForestClassifier(n_jobs=6),
            'et': ExtraTreesClassifier(n_jobs=6),

            'lsvc': LinearSVC(),
            'svc': SVC(probability=True),
            'nusvc': NuSVC(probability=True),
            'xg': 'xg',
            # 'xg':   XGBoostClassifier(),  # Custom
            'knn': KNeighborsClassifier(),
            'logreg': LogisticRegression(),
            'ridge': RidgeClassifier(),
            'sgd': SGDClassifier()
        }

        self.preprocess_dict = {
            'lsvc': self.preprocess_f,
            'nusvc': self.preprocess_f,

            'bag': self.f_bag,
            'knn': self.preprocess_f,
            'rf': self.preprocess_f,
            'xg': self.preprocess_f,


        }

        self.postprocess_dict = {
            #'lsvc': self.postprocess_lsvc,
            #'nusvc': self.postprocess_lsvc,

            'rf': self.postprocess_rf,
            'et': self.postprocess_rf,
            #'knn' : self.postprocess_rf,
        }

    def __init__(self, X, Y, clf_str, params, skf=10, one=True, fold_num=0, skf_seed=11, X_test=None, test_mode='', bag=False, bag_type='simple', alpha=0, pre=[], N=1):

        if type(X) == pd.DataFrame or type(X_test) == pd.DataFrame:
            print 'passed df'
            return
        if type(Y) == pd.Series:
            print 'passed series'
            return

        self.clf_str = clf_str
        self.params = params
        if clf_str == 'xg' and self.params.get('obj') == 'kappa':
            from kappa import KappaRelaxedObjective, kappa_relaxed
            self.params['obj'] = KappaRelaxedObjective()
            self.params['feval'] = kappa_relaxed
            self.params['base_score'] = 0.

        self.one = one
        self.test_only = (skf == 0)
        self.skf_seed = skf_seed
        self.fold_num = fold_num
        self.pre = pre

        # if N > 1 and bag is False:
        #     print 'bag = False, N set to 1'
        #     N = 1

        self.X_t = X_test if X_test is not None else None
        self.predict_test = X_test is not None

        if skf > 0:
            self.skf = self.get_skf(Y, skf)
        self.test_mode = test_mode

        self.need_bag = bag
        self.bag_alpha = alpha
        self.N = N
        self.init_dicts()

        if self.need_bag:
            self.bag_type = bag_type

        self.multiclass = (np.unique(Y).shape[0]) > 2

        self.res = self.try_clf(X, Y, self.X_t)

        np.random.seed(self.skf_seed)

    def try_clf(self, X, Y, X_test):

        clf_str = self.clf_str
        params = self.params

        if not self.one:
            pr_train = np.zeros([X.shape[0], np.unique(Y).shape[0]])

        if self.predict_test:
            pr_test = np.zeros([X_test.shape[0], np.unique(Y).shape[0]])
            print pr_test
            count = 0

        f = self.run_dict[clf_str]

        # if self.test_only:

        #     r_t = f(X, Y, X, Y, X_test)
        #     pr_test = r_t['pr_test']

        #     for i in range(self.N - 1):
        #         r_t = f(X, Y, X, Y, X_test)
        #         pr_test += r_t['pr_test']

        #     return {'pr_test': pr_test / self.N}

        auc_fold = []

        if not self.test_only:
            pr_bags_bags = []
            for it, (train_index, val_index) in enumerate(self.skf):

                if self.one and self.fold_num != it:
                    continue

                #pr = None
                #pr1 = None
                pr_bags = []
                for it_n in range(self.N):
                    #self.params['scale_pos_weight'] = (it_n + 4) / 4.0
                    X_train, X_val = X[train_index], X[val_index]
                    y_train, y_val = Y[train_index], Y[val_index]

                    X_test = self.X_t.copy() if self.X_t is not None else None

                    X_train, X_val, X_test, y_train = self.preprocess_f(
                        X_train,  X_val,  X_test, y_train)

                    if self.need_bag:
                        X_train, X_val, X_test, y_train = self.preprocess_dict[
                            'bag'](X_train,  X_val,  X_test, y_train)

                    rt = f(X_train, y_train, X_val, y_val, X_test)

                    if clf_str in self.postprocess_dict:
                        post_f = self.postprocess_dict[clf_str]
                        post_f(rt)

                    pr_bags.append(rt['pr'])

                    # print it_n, AUC(y_val, np.mean(pr_bags, axis=0)[:,1]), '+'

                    if self.predict_test:
                        pr_test += rt['pr_test']
                        count += 1

                r = {}
                r['y'] = rt['y']
                r['clf'] = rt['clf']

                r['pr_bags'] = pr_bags
                r['pr'] = np.mean(pr_bags, axis=0)
                r['loss'] = log_loss(r['y'], r['pr'])
                r['accuracy'] = accuracy_score(
                    r['y'], np.argmax(r['pr'], axis=1))

                if not self.multiclass:
                    r['auc'] = AUC(r['y'], r['pr'][:, 1])

                    auc_fold.append(r['auc'])

                r['index'] = val_index
                r['train_index'] = train_index
                if self.one:
                    #get_confusion_matrix(r['pr'], r['y'])
                    return r
                else:
                    print 'accuracy', r['accuracy']
                    print 'logloss : ', r['loss']

                    pr_train[val_index, :] = r['pr']
                    pr_bags_bags.append(pr_bags)

            loss = log_loss(Y, pr_train)
            accuracy = accuracy_score(Y, np.argmax(pr_train, axis=1))

            if not self.multiclass:
                auc = AUC(Y, pr_train[:, 1])
                auc_fold.append(auc)
            else:
                auc = 0
                auc_fold = []

        if self.predict_test:

            if count > 0:
                pr_test /= count
            import gc
            if self.test_mode == 'whole':

                # Let test to be val
                self.predict_test = False

                # X_train = X.copy()
                # y_train = Y

                # X_val = self.X_t.copy()
                # Y_val = np.ones(X_val.shape[0], dtype=int)
                # Y_val[0] = 0
                pr_test *= 0

                pr_bags = []
                for it_n in range(self.N):

                    X_train = X
                    y_train = Y

                    X_val = self.X_t
                    Y_val = np.ones(X_val.shape[0], dtype=int)
                    Y_val[0] = 0

                    gc.collect()

                    # X_train, X_val, X_test, y_train = self.preprocess_f(
                    #     X_train,  X_val,  None, y_train)

                    if self.need_bag:
                        X_train, X_val, X_test, y_train = self.preprocess_dict[
                            'bag'](X_train,  X_val,  None, y_train)

                    r_t = f(X_train, y_train, X_val, None, None)
                    pr_test += r_t['pr']
                    pr_bags.append(r_t['pr'])
                pr_test /= self.N

            outd = {
                'pr_test': pr_test,
                'pr_bag': pr_bags
                # 'pr': pr_train,
                # 'loss': loss,
                # 'accuracy': accuracy,
                # 'y': Y,
                # 'auc': auc,
                # 'auc_fold': auc_fold,
            }

            for va in ['pr_train', 'pr', 'loss', 'accuracy', 'auc', 'auc_fold']:
                if va  in locals():
                    outd[va] = eval(va) 

            return outd
        else:

            return {
                'y': Y,
                'pr': pr_train,
                'loss': loss,
                'accuracy': accuracy,
                'auc': auc,
                'auc_fold': auc_fold,
                'pr_bags_bags': pr_bags_bags
            }

    def apply_predict_proba(self, X_train, y_train, X_val, y_val, X_test):

        pr_test = None

        clf = self.clf_dict[self.clf_str]

        clf.set_params(**self.params)

        clf.fit(X_train, y_train)

        pr_val = clf.predict_proba(X_val)

        if len(pr_val.shape) == 1:
            pr_val = np.concatenate(
                [1 - pr_val[:, None], pr_val[:, None]], axis=1)

        auc = -1
        loss_val = -1
        accuracy_val = -1
        if y_val is not None:
            loss_val = log_loss(y_val, pr_val)

            accuracy_val = accuracy_score(y_val, np.argmax(pr_val, axis=1))

            if not self.multiclass:
                auc = AUC(y_val, pr_val[:, 1])
            else:
                auc = 0

        if self.predict_test:
            pr_test = clf.predict_proba(X_test)

            if len(pr_test.shape) == 1:
                pr_test = np.concatenate(
                    [1 - pr_test[:, None], pr_test[:, None]], axis=1)
            # print pr_test.shape
        return {
            'clf': clf,
            'pr': pr_val,
            'y': y_val,
            'loss': loss_val,
            'accuracy': accuracy_val,
            'pr_test': pr_test,  # None if  X_test = none
            'auc': auc
        }

    def apply_decision_function(self, X_train, y_train, X_val, y_val, X_test):

        pr_test = None

        clf = self.clf_dict[self.clf_str]

        clf.set_params(**self.params)

        clf.fit(X_train, y_train)

        pr_val = clf.decision_function(X_val)
        if len(pr_val.shape) == 1:
            pr_val = np.concatenate(
                [1 - pr_val[:, None], pr_val[:, None]], axis=1)


        auc = -1
        loss_val = -1
        accuracy_val = -1
        if y_val is not None:
            loss_val = log_loss(y_val, pr_val)

            accuracy_val = accuracy_score(y_val, np.argmax(pr_val, axis=1))

            if not self.multiclass:
                auc = AUC(y_val, pr_val[:, 1])
            else:
                auc = 0


        if self.predict_test:
            pr_test = clf.decision_function(X_test)

            if len(pr_test.shape) == 1:
                pr_test = np.concatenate(
                    [1 - pr_test[:, None], pr_test[:, None]], axis=1)

        return {
            'clf': clf,
            'pr': pr_val,
            'y': y_val,
            'loss': loss_val,
            'accuracy': accuracy_val,
            'pr_test': pr_test  # None if  X_test = none
        }

    def get_skf(self, Y, n_folds):
        return StratifiedKFold(Y, n_folds=n_folds, shuffle=True, random_state=self.skf_seed)

    
    def apply_xgb(self, X_train, y_train, X_val, y_val, X_test):
        return apply_f.apply_xgb(self.params, X_train, y_train, X_val, y_val)

        # pr_test = None

        # dtrain = xgb.DMatrix(X_train, label=y_train)
        # watchlist = [(dtrain, 'train')]


        # if X_val is not None:
        #     dval = xgb.DMatrix(X_val, label=y_val)
        # if y_val is not None:
        #     watchlist.append((dval, 'eval'))

        # del X_train, X_val

        # self.params['seed'] += np.random.randint(1000)
        
        # params = self.params
        # if 'early_stopping_rounds' in params:
        #     bst = xgb.train(
        #         params, dtrain, params['num_round'], watchlist,early_stopping_rounds = params['early_stopping_rounds'])
        #     print 'bi: ', bst.best_iteration
        #     pr_val = bst.predict(dval,ntree_limit=bst.best_iteration)
        # else:
        #     bst = xgb.train(
        #         params, dtrain, params['num_round'], watchlist)

        #     pr_val = bst.predict(dval)

        # # bst = xgb.train(
        # #     self.params, dtrain, self.params['num_round'], watchlist, feval = self.params.get('feval'), obj = self.params.get('obj'), xgb_model = self.params.get('xgb_model'))

        # pr_val = bst.predict(dval)

        # if len(pr_val.shape) == 1:
        #     pr_val = np.concatenate(
        #         [1 - pr_val[:, None], pr_val[:, None]], axis=1)

        # auc = -1
        # loss_val = -1
        # accuracy_val = -1
        # if y_val is not None:
        #     loss_val = log_loss(y_val, pr_val)

        #     accuracy_val = accuracy_score(y_val, np.argmax(pr_val, axis=1))

        #     if not self.multiclass:
        #         auc = AUC(y_val, pr_val[:, 1])
        #     else:
        #         auc = 0


        # if self.predict_test:
        #     dtest = xgb.DMatrix(X_test)
        #     pr_test = bst.predict(dtest)

        #     if len(pr_test.shape) == 1:
        #         pr_test = np.concatenate(
        #             [1 - pr_test[:, None], pr_test[:, None]], axis=1)

        # return {
        #     'clf': bst,
        #     'pr': pr_val,
        #     'y': y_val,
        #     'loss': loss_val,
        #     'accuracy': accuracy_val,
        #     'pr_test': pr_test,  # None if  X_test = none
        #     'auc': auc
        # }

    # ========+ preprocess
    def f_bag(self, X_train, X_val, X_test, y_train):

        if self.bag_type == 'simple':
            print 'simple'
            bag_idx = np.random.choice(
                range(X_train.shape[0]), X_train.shape[0])
        else:
            print 'alpha'
            bag_idx = range(X_train.shape[0])
            bag_idx.extend(np.random.choice(
                range(X_train.shape[0]), int(X_train.shape[0] * self.bag_alpha)))

        X_train = X_train[bag_idx]
        y_train = y_train[bag_idx]

        return X_train, X_val, X_test, y_train

    def preprocess_rf(self, X_train, X_val, X_test, y_train):
        # return

        # if self.predict_test:
        #     train_test = np.concatenate([X_train, X_val, X_test], axis=0)
        # else:
        #     train_test = np.concatenate([X_train, X_val], axis=0)

        # sc = StandardScaler()
        # sc.fit(train_test)

        # tfidf = TfidfTransformer()
        # tfidf.fit(train_test)

        # X_train = sc.transform(X_train)
        # X_train = tfidf.transform(X_train)

        # X_val = sc.transform(X_val)
        # X_val = tfidf.transform(X_val)

        # if self.predict_test:
        #     X_test = sc.transform(X_test)
            #X_test = tfidf.transform(X_test)

        return X_train, X_val, X_test, y_train

    def preprocess_f(self, X_train, X_val, X_test, y_train):

        if len(self.pre) == 0:
            return X_train, X_val, X_test, y_train

        if self.predict_test:
            if scipy.sparse.issparse(X_train):
                train_test = scipy.sparse.vstack(
                    [X_train, X_val, X_test], format='csr')
            else:
                # print X_test.shape
                # print X_val.shape
                # print X_train.shape
                train_test = np.concatenate([X_train, X_val, X_test], axis=0)
        else:
            if scipy.sparse.issparse(X_train):
                train_test = scipy.sparse.vstack(
                    [X_train, X_val], format='csr')
            else:
                train_test = np.concatenate([X_train, X_val], axis=0)

        for p in self.pre:
            print p
            if p == 'sc':
                sc = StandardScaler()
                train_test = sc.fit_transform(train_test)
                X_train = sc.transform(X_train)
                X_val = sc.transform(X_val)

                if self.predict_test:
                    X_test = sc.transform(X_test)

            if p == 'tfidf':
                tfidf = TfidfTransformer()
                train_test = tfidf.fit_transform(train_test)
                X_train = tfidf.transform(X_train)
                X_val = tfidf.transform(X_val)

                if self.predict_test:
                    X_test = tfidf.transform(X_test)

            if p == 'log1p':
                if scipy.sparse.issparse(X_train):
                    train_test = train_test.log1p()

                    X_train = X_train.log1p()
                    X_val = X_val.log1p()

                    if self.predict_test:
                        X_test = X_test.log1p()
                else:
                    train_test = np.log1p(train_test)
                    X_train = np.log1p(X_train)
                    X_val = np.log1p(X_val)

                    if self.predict_test:
                        X_test = np.log1p(X_test)

        return X_train, X_val, X_test, y_train
        #X_train = sc.fit_transform(X)
        #X_test =  sc.transform(X_test)

        #X = TfidfTransformer().fit_transform(Xs)
        #X_test =  sc.transform(X_test)
        # pass

    # ========= postprocess
    def postprocess_rf(self, r):
        pass
        # power, _, loss = ApplyPower(
        #     r['y_val'], r['pr_val'], log_loss, plot=False)
        # print 'logloss = ', loss, ' power = ', power

    def postprocess_lsvc(self, r):
        pass
