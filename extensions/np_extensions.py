import numpy as np
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from forbiddenfruit import curse


def extend_np(name, function):
    arr = np.array([])
    if hasattr(arr, name):
        return
        # print 'Numpy.array already has a ' + name + ' method'
    else:
        curse(np.ndarray, name, function)


def _np_shuffle(self):
    t = self.copy()
    np.random.shuffle(t)
    return t


def _np_median(self):
    return np.median(self)


def _np_log1p(self):
    return np.log1p(self)


def _np_sc(self):
    return StandardScaler().fit_transform(self)


def _np_sc01(self):
    return MinMaxScaler().fit_transform(self)

extend_np('log1p', _np_log1p)
extend_np('sc', _np_sc)
extend_np('sc01', _np_sc01)
extend_np('median', _np_median)
extend_np('shuffle', _np_shuffle)
