import pandas as pd
import numpy as np
# Extensions

from sklearn.preprocessing import StandardScaler


def extend_df(name, function):
    df = pd.DataFrame({})
    if hasattr(df, name):
        return
        # print 'DataFrame already has a ' + name + ' method'
    else:
        setattr(pd.DataFrame, name, function)


def extend_series(name, function):
    s = pd.Series({})
    if hasattr(s, name):
        return
        # print 'DataFrame already has a ' + name + ' method'
    else:
        setattr(pd.Series, name, function)
        setattr(pd.core.index.Index, name, function)


def _df_shuffle(self, y=None):

    df = self.copy()
    if y is not None:
        df = df[:y.shape[0]]
        df['__tmpy'] = y

    index = list(df.index)
    # np.random.seed(414)
    np.random.shuffle(index)
    df = df.ix[index]
    df.reset_index(inplace=True, drop=True)

    result = df
    if y is not None:
        y = pd.Series(df['__tmpy'], index=df.index)
        df.drop('__tmpy', axis=1, inplace=True)
        result = (df, y)
    return result


def _df_log1p(self):
    df = pd.DataFrame(np.log1p(self))
    df.columns = self.columns
    return df


def _df_sc(self):

    sc = StandardScaler()
    return pd.DataFrame(sc.fit_transform(self.fillna(-1).values),
                        columns=self.columns)
    # for c in self.columns:
    #     self.ix[:, c] = sc.fit_transform(self[c].fillna(-1))
    # return self


def _df_meanl(self, alpha=0, axis=None, **kwargs):

    s = self.sum(axis=axis)
    return (s + alpha) / (s.shape + alpha)


def _series_isinv(self, other, is_not=False):
    mask = self.isin(other)

    return self[~mask] if is_not else self[mask]


def _series_isninv(self, other):
    mask = self.isin(other)

    return self[~mask]
    # s = self.sum(axis=axis)
    # return (s + alpha) / (s.shape + alpha)

extend_df('shuffle', _df_shuffle)
extend_df('log1p', _df_log1p)
extend_df('sc', _df_sc)
extend_df('meanl', _df_meanl)

extend_series('isinv', _series_isinv)
extend_series('isninv', _series_isninv)


# extend_index('isinv', _series_isinv)
# extend_index('isninv', _series_isninv)
