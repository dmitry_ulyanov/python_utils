import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from matplotlib import cm


'''

'''


def rand_jitter(vec):
    vec = np.array(vec)
    stdev = float((np.nanmax(vec) - np.nanmin(vec))) / 200.0
    return vec + np.random.randn(len(vec)) * stdev


def scj(x, y, log1p=False, **kwargs):
    return sc2_(rand_jitter(x), rand_jitter(y), log1p, **kwargs)


def sc2_(x, y, log1p=False, **kwargs):
    if log1p:
        return plt.scatter(np.log1p(x), np.log1p(y),
                           edgecolor='none', **kwargs)
    else:
        return plt.scatter(x, y, edgecolor='none', **kwargs)


def sc2(x, y, log1p=False, **kwargs):
    if type(x) == dict and type(y) == dict:
        sc2_(x['pr'][:, 1], y['pr'][:, 1], log1p, **kwargs)
    else:
        sc2_(x, y, log1p, **kwargs)


def scf(pr1, pr2, y, wh):
    c = ['#ff0000', '#ffff00', '#00ff00', '#00ffff', '#0000ff',
         '#ff00ff', '#990000', '#999900', '#009900', '#009999']

    for i in range(9):
        mask = (y == i)

        plt.scatter(pr1[mask, wh], pr2[mask, wh], c=c[i], edgecolor='none')


def rand_index(vec, many):
    return np.random.choice(len(vec), many, replace=False)


def scr(x, y, many, **kwargs):
    idx = rand_index(x, many)
    c = kwargs.get('c', None)

    if c is None:
        c = 'b'
    else:
        c = c[idx]
        kwargs.pop('c')

    return plt.scatter(x[idx], y[idx],  edgecolor='none', c=c, **kwargs)

def draw_norm_hist(train,test, c):
    traintest= pd.concat([train,test],axis=0)
    for c in int_cols:
        print c 
        plt.figure()
        train[c].hist(bins= 1000, ec='none',normed= True, alpha = 0.5,range=(traintest[c].min(),traintest[c].max()))
        test[c].hist(bins= 1000, ec='none',normed= True,alpha = 0.5,range=(traintest[c].min(),traintest[c].max()))
        plt.show()


'''

'''


def draw_imp(clf, names=None, sorted=True):

    if names is not None:
        if sorted:

            imp_idx = np.argsort(clf.feature_importances_)
            names_ = names[imp_idx]
            impo = clf.feature_importances_[imp_idx]

        else:
            names_ = names
            impo = clf.feature_importances_

        plt.xticks(range(0, len(names_)),
                   names_, rotation=90)
        #plt.grid(True, which="both")
        plt.plot(impo)

        d = pd.DataFrame(names_, columns=['names'])
        d['imp'] = impo
        return d

    plt.plot(clf.feature_importances_)
    return clf.feature_importances_


def confusion_probs(predictions, labels, ax=None, vmargin=0.1, equalize=False, cmap=cm.jet):
    """
        Make a confusion-matrix-like scatter plot
    """
    N, D = predictions.shape
    bins = np.bincount(labels)
    assert labels.min() >= 0 and labels.max() < D, 'labels should be between 0 and {0:d}'.format(D - 1)
    assert predictions.min() >= 0 and predictions.max() <= 1, 'predictions should be normalized (to [0, 1])'
    
    """ Set up graphical stuff. """
    fig = None
    if ax is None:
        fig, ax = plt.subplots(figsize=(16, 16))
        fig.tight_layout()
    if equalize:
        xticks = [x + .5 for x in range(D)]
    else:
        xticks = (float(D) / N) * (np.cumsum(bins) - bins / 2)
    ax.set_xticks(xticks)
    ax.set_xticklabels(range(D))
    ax.set_xlim([0, D])
    ax.set_yticks([(x + .5) * (1 + vmargin) for x in range(D)])
    ax.set_yticklabels(range(D))
    ax.set_ylim([0, D * (1 + vmargin) - vmargin])
    """ Sort predictions by label. """
    indices = labels.argsort()
    X = predictions[indices, :]
    y = labels[indices]
    """ Add more vertical offset to higher labels. """
    X += np.linspace(0, D - 1, D) * (1 + vmargin)
    """ Make a new horizontal axis to give all samples equal width. """
    if equalize:
        z = np.hstack([k + np.linspace(0, 1, t) for k, t in enumerate(bins)])
    else:
        z = np.linspace(0, D, N)
    """ Plot the results with class-coding. """
    for q in range(D):
        colors = cmap(y / float(D))
        ax.scatter(z, X[:, q], c=colors, edgecolors='none')
    # return (fig, ax) if fig else ax
