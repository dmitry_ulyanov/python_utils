from sklearn import preprocessing
import numpy as np
import pandas as pd
from to_series import to_series


def _re(train_data_col_, test_data_col_, filler=-1, min_freq=None):

    train_data_col = to_series(train_data_col_)
    test_data_col = to_series(test_data_col_)

    train_vc = train_data_col.value_counts()
    test_vc = test_data_col.value_counts()

    redundant_values_in_train = train_vc.index[
        ~train_vc.index.isin(test_vc.index)]
    unseen_values_in_test = test_vc.index[~test_vc.index.isin(train_vc.index)]
    #intersection = train_vc.index[train_vc.index.isin(test_vc.index)]

    train_data_col[train_data_col.isin(redundant_values_in_train)] = filler
    test_data_col[test_data_col.isin(unseen_values_in_test)] = filler

    if min_freq is not None:
        infreq = train_vc[train_vc < min_freq].index

        train_data_col[train_data_col.isin(infreq)] = filler
        test_data_col[test_data_col.isin(infreq)] = filler

    return train_data_col.values, test_data_col.values


'''
1) Finds levels in train, that do not exist in test. Encodes them as `filler`.
2) Finds levels in test, that do not exist in train. Encodes them as `filler`.
3) Finds levels with freq less that `min_freq`.      Encodes them as `filler`.


data_col_:
        something, that can be converted to series
mask:
        data_col_[mask] is the train set

== returns: 
    np.array

'''


def _re1(data_col_, mask, filler=-1, min_freq=None):

    data_col_ = to_series(data_col_)

    train_data_col_ = data_col_[mask]
    test_data_col_ = data_col_[~mask]

    train_data_col, test_data_col = _re(
        train_data_col_, test_data_col_, filler, min_freq)

    t = data_col_.copy()
    t[mask] = train_data_col
    t[~mask] = test_data_col

    return t
