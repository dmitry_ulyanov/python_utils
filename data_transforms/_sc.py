from sklearn.preprocessing import StandardScaler
import numpy as np
import pandas as pd
from to_series import to_series

def _sc(data_train, data_test=None):


    sc = StandardScaler(copy=True)
    print(1)
    
    if data_test is not None:
        sc.fit(np.concatenate([data_train,data_test]))

        return sc.transform(data_train), sc.transform(data_test)
    else:
        return  sc.fit_transform(data_train)
