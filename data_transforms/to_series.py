import numpy as np
import pandas as pd

'''
Converts `data` to pd.Series

data:
        one of the type:
        -- Series
        -- DF (N,1)
        -- NDarray (N,)
        -- NDarray (N,1)
'''


def to_series(data):

    if type(data) is pd.Series:
        return data.copy()

    if type(data) is pd.DataFrame:
        if data.shape[1] == 1:
            return data.iloc[:, 0]
        else:
            raise Exception('DataFrame passed, but the shape is not (N,1)')

    if type(data) is np.ndarray:
        if len(data.shape) == 1:  # shape == (N, )
            return pd.Series(data)
        elif len(data.shape) == 2:
            if data.shape[1] == 1:
                return pd.Series(data[:, 0])
            else:
                raise Exception('NDarray passed, but the shape is not (N,1)')
        else:
            raise Exception('NDarray passed, but number of dims > 2')

    raise Exception("What the hell you've passed ?")
