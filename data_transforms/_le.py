from sklearn import preprocessing
import numpy as np
import pandas as pd
from to_series import to_series

'''
Encodes unique values of the vector by [0..N]. 
Concatenates `data_train`, `data_test` first if `data_test` is provided to ensure consistency.

data_train:
        something, that can be converted to series

data_test:
        something, that can be converted to series


smart:
        'no' -- in order: [3 1 5 5] -> [1 0 2 2] ; NaN -> max+1
        'c'  -- by counts [3 1 5 5] -> [1 2 0 0]
        'm'  -- by category mean of Y
labels:
        Y, for 'm' mode

fillna:
        NaNs encoding method for 'c' and 'm' modes
'''


def _le(data_train, data_test=None, smart='no', labels=None, fillna='max+1', return_le=False):
    if smart == 'm' and labels is None:
        raise Exception('smart == m but labels are not set')

    data_train = to_series(data_train)

    # Concat data if need
    if data_test is not None:
        data = pd.concat(
            [data_train, to_series(data_test)]).reset_index(drop=True)
    else:
        data = data_train
    # ===================

    if smart in ['c', 'm']:

        if smart == 'c':
            vc = data.value_counts()
        elif smart == 'm':
            labels = to_series(labels)

            # assume NaN correspondes to test records
            # looks like groupby.mean omits NaNs

            vc = labels.groupby(data).mean()

            vc.sort()

        dic = {key: i for i, key in enumerate(vc.index.tolist())}

        encoded = data.map(dic)
        if fillna == '-1':
            encoded.fillna(-1, inplace=True)
        if fillna == 'max+1':
            encoded.fillna(encoded.max() + 1, inplace=True)

        encoded = encoded.values
        # if data_transform is not None:
        #     return d, data_transform.map(dic)
    # elif smart in ['r']:
    # it is the same as _le() as it turned out
    #     u = data.unique()
    #     d = np.sort(u)

    #     return data.map({d[i]: i for i in range(d.shape[0])})
       # return data.argsort()  # makes nans as -1
        lenc = None
    else:
        # labels in order, NaN will be encoded as `max+1`

        lenc = preprocessing.LabelEncoder()
        encoded = lenc.fit_transform(data)

    if data_test is not None:
        if return_le: 
            return encoded[:data_train.shape[0]], encoded[data_train.shape[0]:], lenc
        else:
            return encoded[:data_train.shape[0]], encoded[data_train.shape[0]:]
    else:
        if return_le: 
            return encoded, lenc
        else:
            return encoded
