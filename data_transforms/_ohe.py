from sklearn import preprocessing
import numpy as np
import pandas as pd
from _le import _le
from to_series import to_series

'''
One-hots the vector.

data:
        something, that can be converted to series
data_transform:
        fit on data, transform this


P.S. OneHotEncoder need matrix, so everything is converted to NDarray (N,1)
'''


def _ohe_transform(data, data_transform=None, need_le=False):

    if data_transform is not None:
        data_transform_ = to_series(data_transform).values[:, None]

    if need_le:
        if data_transform is None:
            data_ = _le(data, data_transform=data_transform, smart='c')[
                :, None]
        else:
            data_, data_transform_ = _le(
                data, data_transform=data_transform, smart='no')[:, None]
    else:
        data_ = to_series(data).values[:, None]

    oh = preprocessing.OneHotEncoder()
    d = oh.fit_transform(data_)
    if data_transform is not None:
        return d, oh.transform(data_transform_)
    return d


'''
One-hot encodes the vector.

data_train:
        something, that can be converted to series

data_test:
        something, that can be converted to series

need_le:
        if the data has not been converted to INT categories yet

P.S. OneHotEncoder need matrix, so everything is converted to NDarray (N,1)
'''


def _ohe(data_train, data_test=None, need_le=False):

    data_train = to_series(data_train)

    # Concat data if need
    if data_test is not None:
        data = pd.concat(
            [data_train, to_series(data_test)]).reset_index(drop=True)
    else:
        data = data_train
    # ===================

    data = data.values[:, None]

    if need_le:
        data = _le(data)[:, None]

    oh = preprocessing.OneHotEncoder()
    encoded = oh.fit_transform(data)

    if data_test is not None:
        return encoded[:data_train.shape[0]], encoded[data_train.shape[0]:]
    else:
        return encoded

def _ohe_append(train, test, col_name, original = 'leave'):

    train_le, test_le,le   = _le(train[col_name], test[col_name],return_le=True)
    train_ohe, test_ohe = _ohe(train_le, test_le)

    for i in range(train_ohe.shape[1]):
        train[col_name + '_' + le.classes_[i]] = train_ohe[:,i].toarray() 
        test[col_name + '_' + le.classes_[i]] = test_ohe[:,i].toarray()
        
    if original == 'drop':
        train.drop(col_name,axis=1,implace  =True)
        test.drop(col_name,axis=1,implace  =True)
    elif original == 'le':
        train[col_name] = train_le
        test[col_name] = test_le
    return train, test