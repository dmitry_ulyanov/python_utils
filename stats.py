import pandas as pd
import numpy as np

'''
for c in train.columns: 
    print c
    stats(train, test, c)
'''


def col_stats(train, test, column):

    train_data_col = train[column]
    test_data_col = test[column]

    train_vc = train_data_col.value_counts()
    test_vc = test_data_col.value_counts()

    redundant_values_in_train = train_vc.index[
        ~train_vc.index.isin(test_vc.index)]
    unseen_values_in_test = test_vc.index[~test_vc.index.isin(train_vc.index)]
    intersection = train_vc.index[train_vc.index.isin(test_vc.index)]
    print column
    print "\n /*==================== \n"
    print train_data_col.describe().to_frame(column + ':training').T.append(test_data_col.describe().to_frame(column + ':test').T)

    print "\n"

    print "Intersection in            ", intersection.shape[0], " values"
    print "Redundant values in train: ", redundant_values_in_train.shape[0], " values"
    print "Not seen values in train:  ", unseen_values_in_test.shape[0], " values"

    print "\n ====================*/ \n"
