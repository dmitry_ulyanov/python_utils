
# coding: utf-8

# # Get path for this

# In[1]:

get_ipython().run_cell_magic(u'javascript', u'', u'var nb = IPython.notebook;\nvar kernel = IPython.notebook.kernel;\nvar command = "NOTEBOOK_FULL_PATH = \'" + nb.base_url + nb.notebook_path + "\'";\nkernel.execute(command);')


# In[9]:

a = get_ipython().getoutput(u'pwd')

path = a[0] + NOTEBOOK_FULL_PATH
print "NOTEBOOK_FULL_PATH:\n", path


# # List this one

# In[13]:

from nbformat import current
import io
with io.open(path) as f:
    nb = current.read(f, 'json')
    

ip = get_ipython()

for i,cell in enumerate(nb.worksheets[0].cells):
    if cell.cell_type != 'code':
        continue
    print "\n=========", i,'\n'
    print cell.input
    #ip.run_cell(cell.input


# # Execute some

# In[16]:

to_exec = [1,2]
ress = []
for i in range(10):
    for j in to_exec:
        ip.run_cell(nb.worksheets[0].cells[j].input)

