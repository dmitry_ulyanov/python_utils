'''
           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                   Version 2, December 2004

Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.

           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.
'''


from datetime import datetime
from csv import DictReader
from math import exp, sqrt
import argparse
import random
import gc
import os
from ftpls.ftpl_mse_no_hash import ftrl_proximal
import shutil
import pickle

parser = argparse.ArgumentParser()
parser.add_argument("--alpha")
parser.add_argument("--beta")
parser.add_argument("--L1")
parser.add_argument("--L2")
parser.add_argument("--D")
parser.add_argument("--epoch")
parser.add_argument("--oob")
parser.add_argument("--prefix")
parser.add_argument("--train_range")
parser.add_argument("--test")
parser.add_argument("--seed", default='0')
parser.add_argument("--start_day")
parser.add_argument("--counts")
parser.add_argument("--auto_mode")
parser.add_argument("--test_auto_mode")
#.add_argument("--alpha_drop")

parser.add_argument("--autoreg_start")
parser.add_argument("--autoreg_len")

parser.add_argument("--load_model", default='')

#aplha_drop = float(args.alpha_drop)

args = parser.parse_args()

test_auto_mode = args.test_auto_mode
auto_mode = args.auto_mode

if args.counts != '':
    day_counts = [int(x) for x in args.counts.split(',')]
else:
    day_counts = []
##############################################################################
# parameters #################################################################
##############################################################################

load_model = args.load_model

data_dir = '../data/'

itId_path = data_dir + 'for_ftpl/train_itId.csv'
ads_path = data_dir + 'for_ftpl/train_ads.csv'
test_path = data_dir + 'for_ftpl/test_reduced.csv'

autoreg_len = int(args.autoreg_len)
autoreg_start = int(args.autoreg_start)

epoch_num = int(args.epoch)

alpha = float(args.alpha)  # learning rate
beta = float(args.beta)   # smoothing parameter for adaptive learning rate
L1 = float(args.L1)    # L1 regularization, larger value means more regularized
L2 = float(args.L2)    # L2 regularization, larger value means more regularized


D = 2 ** int(args.D)           # number of weights to use

oob = float(args.oob)

prefix = args.prefix
train_dates_range = int(args.train_range)

need_test = (args.test == 'True') or (train_dates_range == 151)

seed = int(args.seed)
interaction = False
start_day = int(args.start_day)


def mkdirp(directory):
    if not os.path.isdir(directory):
        os.makedirs(directory)

exec_dir = '../data/ftpl_data/' + prefix + '/'

mkdirp(exec_dir)
shutil.copyfile('ftpl.py', exec_dir + 'ftpl.py')

##############################################################################
# class, function, generator definitions #####################################
##############################################################################

ItIdcols = ['AdId', 'Date', 'Slot', 'Device']
ranges = {
    'AdId': 52917,
    'Date': train_dates_range,
    'Device': 3,
    'Slot': 4
}

imp_counts = [{} for x in range(len(day_counts))]

preds = {}

train_first_time = True
test_first_time = True
val_first_time = True


def augment(cur_data):
    #bag_AdGroupName = cur_data['AdGroupName'].split(' ')
    #bag_CampaignName = cur_data['CampaignName'].split(' ')
    #bag_KeywordText = cur_data['KeywordText'].split(' ')

    bags = {}
    # bags['AdGroupName'] = bag_AdGroupName
    #bags['KeywordText'] = bag_KeywordText

    # del cur_data['AdNetworkType2']
    # del cur_data['KeywordMatchType']

    # del cur_data['DestinationUrl']

    cur_data['weekday'] = str(int(cur_data['Date']) % 7)

    cur_data['w_s'] = cur_data[
        'weekday'] + '_s_' + cur_data['Slot']
    cur_data['w_d'] = cur_data[
        'weekday'] + '_d_' + cur_data['Device']

    # cur_data['w_a'] = cur_data[
    #     'weekday'] + '_a_' + cur_data['AdId']

    del cur_data['Date']

    cur_data['fif'] = cur_data['AdId'] + '_' + \
        cur_data['Slot'] + cur_data['Device']

    cur_data['w_f'] = cur_data[
        'weekday'] + '_f_' + cur_data['fif']

    #del cur_data['weekday']

    # for i, dc in enumerate(day_counts):
    #     cur_data[str(dc) + '_dc_f'] = cur_data[
    #         str(dc) + 'd_count'] + '_f_' + cur_data['fif']

    # == autoreg
    for i in range(1, autoreg_len + 1):
        if 'auto_' + str(i) in cur_data:
            cur_data[
                'slot_auto' + str(i)] = cur_data['Slot'] + '_' + cur_data['auto_' + str(i)]
        #del cur_data['auto_'+str(i)]
    # ==

    #del cur_data['Device']
    #del cur_data['Slot']
    #del cur_data['AdId']
    #del cur_data['fif']

    return cur_data, bags


def gen_data_val():
    global val_first_time

    for cur_ad in range(ranges['AdId']):
        for slot in range(ranges['Slot']):
            for device in range(ranges['Device']):
                for date in range(train_dates_range, 151):

                    cur_data = ads[str(cur_ad)].copy()
                    cur_data['Device'] = str(device)
                    cur_data['Slot'] = str(slot)
                    cur_data['Date'] = str(date)

                    r = [str(cur_ad), str(date), str(slot), str(device)]

                    # == count
                    id_no_date = '_'.join(
                        [str(cur_ad), str(slot), str(device)])
                    for i, dc in enumerate(day_counts):
                        count = imp_counts[i].get(id_no_date, 0)
                        cur_data[str(dc) + 'd_count'] = str(count)
                    # ==

                    # == Autoreg
                    autos = get_autoreg(date, r, test_auto_mode)
                    for i, v in enumerate(autos):
                        cur_data['auto_' + str(i + 1)] = v

                    cost = 0
                    if '_'.join(r) in it_ids:
                        cost = it_ids['_'.join(r)]

                    cur_data, bags = augment(cur_data)

                    x = learner.build_x(cur_data, bags)
                    if val_first_time:
                        print cur_data.keys()
                        val_first_time = False

                    yield x, cost, '_'.join(r)  # , cur_ad, slot, device, date


def gen_data_test():
    global test_first_time

    for row in DictReader(open(test_path)):

        cur_ad = row['AdId']
        slot = row['Slot']
        date = row['Date']
        device = row['Device']

        cur_data = ads[str(cur_ad)].copy()

        # == count
        id_no_date = '_'.join([row['AdId'], row['Slot'], row['Device']])
        for i, dc in enumerate(day_counts):
            count = imp_counts[i].get(id_no_date, 0)
            cur_data[str(dc) + 'd_count'] = str(count)
        # ==

        # == Autoreg
        r = [cur_ad, date, slot, device]
        autos = get_autoreg(int(date), r, test_auto_mode)
        for i, v in enumerate(autos):
            cur_data['auto_' + str(i + 1)] = v

        cur_data['Device'] = device
        cur_data['Slot'] = slot
        cur_data['Date'] = date

        cur_data, bags = augment(cur_data)
        x = learner.build_x(cur_data, bags)

        if test_first_time:
            print cur_data.keys()
            test_first_time = False

        yield x, row['Id'], '_'.join(r)


def get_autoreg(date, r, autos_mode):

    autoregs = []
    # == autoreg V2
    for i in range(autoreg_start, autoreg_start + autoreg_len):
        # === actuals
        if date >= i:
            r_temp = r * 1
            r_temp[1] = str(date - i)
            if ('_'.join(r_temp) in it_ids):
                auto_cost = it_ids['_'.join(r_temp)]
            else:
                auto_cost = 0.0
        else:
            auto_cost = 0.0

        # === preds
        if date >= i:
            r_temp = r * 1
            r_temp[1] = str(date - i)

            if ('_'.join(r_temp) in preds):
                auto_pred_cost = preds['_'.join(r_temp)]
            else:
                auto_pred_cost = 0.0
        else:
            auto_pred_cost = 0.0

        if autos_mode == 'linear':
            alpha = float(date - start_day) / \
                (train_dates_range - start_day - 1)
        elif autos_mode == 'preds':
            alpha = 1.0
        elif autos_mode == 'mix_val':
            if date - i < train_dates_range:
                alpha = 0.0
            else:
                alpha = 1.0

        elif autos_mode == 'actuals':
            alpha = 0.0
        else:
            raise Exception('autos_mode', 'badd')

        if date >= i:
            autoregs.append(
                str(int(round(auto_cost * (1 - alpha) + auto_pred_cost * alpha))))

    # print autoregs
    return autoregs
    # ==


def gen_data_fair(start_, end_):
    global imp_counts
    global train_first_time

    imp_counts = [{} for x in range(len(day_counts))]

    for date in range(start_, end_):
        train_index = range(ranges['AdId'] * ranges['Slot'] * ranges['Device'])

        random.shuffle(train_index)

        for ind in train_index:
            l = unravel_index(
                ind, [ranges['AdId'], ranges['Slot'], ranges['Device']])

            r = [str(l[0]), str(date), str(l[1]), str(l[2])]

            cur_ad = r[0]

            cost = 0
            cur_data = ads[cur_ad].copy()

            # == counts
            id_no_date = '_'.join([str(l[0]), str(l[1]), str(l[2])])

            for i, dc in enumerate(day_counts):
                count = imp_counts[i].setdefault(id_no_date, 0)
                cur_data[str(dc) + 'd_count'] = str(count)

            if '_'.join(r) in it_ids:
                cost = it_ids['_'.join(r)]

                # add this day
                for i, dc in enumerate(day_counts):
                    imp_counts[i][id_no_date] += 1

            # substract
            for i, dc in enumerate(day_counts):
                if date >= dc:
                    r_temp = r * 1
                    r_temp[1] = str(date - dc)
                    imp_counts[i][id_no_date] -= ('_'.join(r_temp) in it_ids)
            # ==

            # == Autoreg
            autos = get_autoreg(date, r, auto_mode)
            for i, v in enumerate(autos):
                cur_data['auto_' + str(i + 1)] = v

            device = r[3]
            slot = r[2]

            cur_data['Device'] = device
            cur_data['Slot'] = slot
            cur_data['Date'] = date

            cur_data, bags = augment(cur_data)
            x = learner.build_x(cur_data, bags)

            if train_first_time:
                print cur_data.keys()
                train_first_time = False

            yield x, cost, '_'.join(r), date


def get_data():

    ads = {}

    for t, row in enumerate(DictReader(open(ads_path))):
        ad_id = row['AdId']

        row['KeywordText'] = row['KeywordText'].replace('+', '')

        del row['AdGroupName']
        del row['CampaignName']
        del row['KeywordText']

        # del row['KeywordId']
        # del row['CampaignId']
        # del row['AdGroupId']

        # del row['AdNetworkType2']
        del row['KeywordMatchType']

        del row['DestinationUrl']

        ads[ad_id] = row

    it_ids = {}
    for t, row in enumerate(DictReader(open(itId_path))):
        AdId = row['ItId']
        # del row['ItId']

        it_ids[AdId] = float(row['Cost'])

    gc.collect()
    return ads, it_ids


def unravel_index(ind, dims):
    out = []
    m = 1
    for d in dims:
        m *= d
    if ind >= m:
        raise Exception('spam', 'eggs')
    for d in dims[::-1]:
        out.append(ind % d)
        ind = ind / d
    return out[::-1]


def update_progress(progress):
    print "\rProgress: [{0:50s}] {1:.1f}%".format('#' * int(progress * 50),
                                                  progress * 100),

##############################################################################
# start training #############################################################
##############################################################################

ads, it_ids = get_data()

random.seed(seed)

start = datetime.now()

# initialize ourselves a learner
if load_model == '':
    learner = ftrl_proximal(alpha, beta, L1, L2, D, interaction)
else:
    learner, preds = pickle.load(open(load_model, 'r'))

with open(exec_dir + 'args', 'w') as f_args:
    f_args.write("%s\n" % args)


f_log = open(exec_dir + 'log.csv', 'w')
for e in range(epoch_num):
    train_loss = 0
    train_count = 0

    loss = 0
    count = 0

    for it, (x, y, itId, date) in enumerate(gen_data_fair(start_day, train_dates_range)):
        p = learner.predict(x)

        if p > 0.0:
            preds[itId] = p
        elif itId in preds:
            del preds[itId]
            # print '!=0 train'

        if (random.random() > oob):
# train

            train_loss += learner.loss(p, y)
            train_count += 1

            learner.update(x, p, y)
        else:
# oob
            loss += learner.loss(p, y)
            count += 1

        if (it % 100000 == 0):

            tl = sqrt(train_loss / (train_count + 0.00001))
            toob = sqrt(loss / (count + 0.00001))

            print ' -epoch : %d - it : %d, oob loss: %f, train loss: %f' % (e, it, toob, tl)
            # print ' - elapsed time: %s' % str(datetime.now() - start)
            f_log.write('%d,%d,%d,%f\n' % (0, e, it, tl))
            f_log.write('%d,%d,%d,%f\n' % (1, e, it, toob))

    # Validation
#    if (it % 10000000 == 0 and it != 0):
    gc.collect()
    val_loss = 0
    val_count = 0
    with open(exec_dir + 'val_e_' + str(e), 'w') as outfile:
        for x, y, itId in gen_data_val():
            p = learner.predict(x)
            if p > 0.0:
                preds[itId] = p
            elif itId in preds:
                del preds[itId]

            val_loss += learner.loss(p, y)
            val_count += 1
            outfile.write('%s,%s\n' % (str(p), str(y)))

    f_log.write('%d,%d,%d,%f\n' %
                (2, e, -1, sqrt(val_loss / (val_count + 0.00001))))
    print ' ====== validation loss: %f ' % sqrt(val_loss / (val_count + 0.00001))
    gc.collect()

    #
    pickle.dump((learner, preds), open(exec_dir + 'learner_e_' + str(e), 'w'))

    # Test
    if need_test:
        with open(exec_dir + 'test_e_' + str(e), 'w') as outfile:
            outfile.write('Id,Cost\n')
            for x, x_id, itId in gen_data_test():
                p = learner.predict(x)
                if p > 0.0:
                    preds[itId] = p
                elif itId in preds:
                    del preds[itId]

                outfile.write('%s, %s\n' % (x_id, str(exp(p) - 1)))
            with open('../data/for_ftpl/pred_appendix', 'r') as appendix:
                for line in appendix:
                    outfile.write('%s\n' % line)
        gc.collect()

f_log.close()
