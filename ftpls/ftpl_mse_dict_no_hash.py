from math import sqrt


class ftrl_proximal(object):

    ''' Our main algorithm: Follow the regularized leader - proximal

        In short,
        this is an adaptive-learning-rate sparse logistic-regression with
        efficient L1-L2-regularization

        Reference:
        http://www.eecs.tufts.edu/~dsculley/papers/ad-click-prediction.pdf
    '''

    def __init__(self, alpha, beta, L1, L2, D, interaction):
    # parameters
        self.alpha = alpha
        self.beta = beta
        self.L1 = L1
        self.L2 = L2

        # feature related parameters
        self.D = D
        self.interaction = interaction

        # model
        # n: squared sum of past gradients
        # z: weights
        # w: lazy weights
        self.n = {}
        self.z = {}
        self.w = {}

        self.feat_dict = {}
        
    def _indices(self, x):
        ''' A helper generator that yields the indices in x

            The purpose of this generator is to make the following
            code a bit cleaner when doing feature interaction.
        '''

        # first yield index of the bias term
        yield 0

        # then yield the normal indices
        for index in x:
            yield index

        # now yield interactions (if applicable)
        if self.interaction:
            D = self.D
            L = len(x)

            x = sorted(x)
            for i in xrange(L):
                for j in xrange(i + 1, L):
                    # one-hot encode interactions with hash trick
                    f_name = str(x[i]) + '_' + str(x[j])

                    # new feat index if we didn't see this, existing otherwise
                    yield self.feat_dict.setdefault(f_name, len(self.feat_dict) + 1)

    def predict(self, x):
        ''' Get probability estimation on x

            INPUT:
                x: features

            OUTPUT:
                probability of p(y = 1 | x; w)
        '''

        # parameters
        alpha = self.alpha
        beta = self.beta
        L1 = self.L1
        L2 = self.L2

        # model
        n = self.n
        z = self.z
        w = {}

        # wTx is the inner product of w and x
        wTx = 0.
        for i in self._indices(x):
            z_i = z.get(i, 0)
            n_i = n.get(i, 0)

            sign = -1. if z_i < 0 else 1.  # get sign of z[i]

            # build w on the fly using z and n, hence the name - lazy weights
            # we are doing this at prediction instead of update time is because
            # this allows us for not storing the complete w
            if sign * z_i <= L1:
                # w[i] vanishes due to L1 regularization
                w[i] = 0.
            else:
                # apply prediction time L1, L2 regularization to z and get w
                w[i] = (sign * L1 - z_i) / ((beta + sqrt(n_i)) / alpha + L2)

            wTx += w[i]

        # cache the current w for update stage
        self.w = w

        # bounded sigmoid function, this is the probability estimation
        return max(wTx, 0)

    def update(self, x, p, y):
        ''' Update model using x, p, y

            INPUT:
                x: feature, a list of indices
                p: click probability prediction of our model
                y: answer

            MODIFIES:
                self.n: increase by squared gradient
                self.z: weights
        '''

        # parameter
        alpha = self.alpha

        # model
        n = self.n
        z = self.z
        w = self.w

        # gradient under logloss
        g = p - y

        # update z and n
        for i in self._indices(x):
            n_i = n.get(i, 0)
            sigma = (sqrt(n_i + g * g) - sqrt(n_i)) / alpha
            z[i] = z.get(i, 0) + g - sigma * w[i]
            n[i] = n.get(i, 0) + g * g

    def loss(self, p, y):
        ''' FUNCTION: mse

            INPUT:
                p: our prediction
                y: real answer

            OUTPUT:
                logarithmic loss of p given y
        '''

        return (p - y)**2

    def build_x(self, cur_data, bags):
        x = []
        
        for key in cur_data:
            value = cur_data[key]

            f_name = key + '_' + value

            # new feat index if we didn't see this, existing otherwise
            index = self.feat_dict.setdefault(
                f_name, len(self.feat_dict) + 1)
            x.append(index)

        for key, value in bags.items():
            for b in value:

                f_name = key + '_' + b
                index = self.feat_dict.setdefault(
                    f_name, len(self.feat_dict) + 1)

                x.append(index)
        return x
