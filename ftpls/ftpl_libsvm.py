'''
           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                   Version 2, December 2004

Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.

           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.
'''


from datetime import datetime
from csv import DictReader
from math import exp, log, sqrt
import argparse
import random
import gc

parser = argparse.ArgumentParser()
parser.add_argument("--alpha")
parser.add_argument("--beta")
parser.add_argument("--L1")
parser.add_argument("--L2")
parser.add_argument("--D")
parser.add_argument("--epoch")
parser.add_argument("--dropout")
parser.add_argument("--sample")
parser.add_argument("--feat_sep")
parser.add_argument("--oob")
#parser.add_argument("--submission_folder")
parser.add_argument("--seed")

args = parser.parse_args()

# TL; DR, the main training process starts on line: 250,
# you may want to start reading the code from there


##############################################################################
# parameters #################################################################
##############################################################################

data_dir = '../data/'

# ============= Train on raw features
train_path = [data_dir + 'train_new.csv']
test_path = data_dir + 'encoded_categories/test/encoded_test.csv'
#==============


alpha = float(args.alpha)  # learning rate
beta = float(args.beta)   # smoothing parameter for adaptive learning rate
L1 = float(args.L1)    # L1 regularization, larger value means more regularized
L2 = float(args.L2)    # L2 regularization, larger value means more regularized

seed = int(args.seed)

sample = args.sample == 'True'
dropout = args.dropout == 'True'

# C, feature/hash trick
D = 2 ** int(args.D)           # number of weights to use
interaction = False     # whether to enable poly2 feature interactions

# D, training/validation
epoch = int(args.epoch)    # learn training data for N passes

feat_sep = args.feat_sep
oob = float(args.oob)
store_val = False

# path of to be outputted submission file
predictions_path = 'ans.csv'

submission_folder = args.submission_folder


##############################################################################
# class, function, generator definitions #####################################
##############################################################################


from ftpls.ftpl_logloss import ftrl_proximal


def get_object(path, sample=False, dropout=False, use_dummy_id=False):
    ''' GENERATOR: Apply hash-trick to the original csv row
                   and for simplicity, we one-hot-encode everything

        INPUT:
            path: path to training or testing file
            D: the max index that we can hash to

        YIELDS:
            ID: id of the instance, mainly useless
            x: a list of hashed and one-hot-encoded 'indices'
               we only need the index since all values are either 0 or 1
            y: y = 1 if we have a click, else we have y = 0
    '''
    for line in open(path):
        process_data(line)
        
        tokens = line.split(' ')
        
        y = float(tokens[0])
        
        if sample and random.random() < 0.1:
            continue

        x = []
        for item in tokens[1:]:
            a = itme.split(':')
            field = a[0]
            feat_num = a[1]
            value = a[2]
            
            x.app
            
        learner.build_x(cur_data, bags)
        yield x, y

        
def build_x(self, cur_data, bags):
        x = []
        
        for key in cur_data:
            value = cur_data[key]

            f_name = key + '_' + value

            # new feat index if we didn't see this, existing otherwise
            index = self.feat_dict.setdefault(
                f_name, len(self.feat_dict) + 1)
            x.append(index)

        for key, value in bags.items():
            for b in value:

                f_name = key + '_' + b
                index = self.feat_dict.setdefault(
                    f_name, len(self.feat_dict) + 1)

                x.append(index)
        return x

##############################################################################
# start training #############################################################
##############################################################################
random.seed(seed)

start = datetime.now()

# initialize ourselves a learner
learner = ftrl_proximal(alpha, beta, L1, L2, D, interaction)

for e in range(epoch_num):
    train_loss = 0
    train_count = 0

    loss = 0
    count = 0

    for it, (x, y) in enumerate(get_row(train_path)):
        p = learner.predict(x)

        if p > 0.0:
            preds[itId] = p
        elif itId in preds:
            del preds[itId]
            # print '!=0 train'

        if (random.random() > oob):
# train

            train_loss += learner.loss(p, y)
            train_count += 1

            learner.update(x, p, y)
        else:
# oob
            loss += learner.loss(p, y)
            count += 1


# start training
for e in xrange(epoch):
    loss = 0.
    count = 0

    train_loss = 0.
    train_count = 0.
    val_scores = []
    for train_cur in train:
        gc.collect()
        # data is a generator
        for t, date, ID, x, y in data(train_cur, D, sample=sample, dropout=dropout, use_dummy_id=True):
        #    t: just a instance counter
        # date: you know what this is
        #   ID: id provided in original data
        #    x: features
        #    y: label (click)

        # step 1, get prediction from learner

            p = learner.predict(x)

            if (random.random() > oob):
                # train

                train_loss += logloss(p, y)
                train_count += 1

                learner.update(x, p, y)
            else:
                if store_val:
                    val_scores.append(p)
                    # test
                    loss += logloss(p, y)
                    count += 1

            if (t % 1000000 == 0):
                gc.collect()

    print 'Epoch %d finished' % e
    print ' - validation loss: %f ' % (loss / (count + 0.0001))
    print ' - train loss: %f ' % (train_loss / (train_count + 0.0001))
    print ' - elapsed time: %s' % str(datetime.now() - start)

    if store_val:
        with open(submission_folder + feat_sep + '_val_' + str(e), 'w') as outfile:
            outfile.write('click\n')
            for sc in val_scores:
                outfile.write('%s\n' % (str(sc)))

    print 'Predicting on 30\n'
    with open(submission_folder + feat_sep + '_30_' + str(e), 'w') as outfile:
        outfile.write('id,click\n')
        for t, date, ID, x, y in data(val_test, D):
            p = learner.predict(x)
            outfile.write('%s,%s\n' % (ID, str(p)))

    print 'Predicting on test\n'
    with open(submission_folder + feat_sep + '_' + str(e), 'w') as outfile:
        outfile.write('id,click\n')
        for t, date, ID, x, y in data(test, D):
            p = learner.predict(x)
            outfile.write('%s,%s\n' % (ID, str(p)))
