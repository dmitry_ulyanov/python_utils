from sklearn.metrics import mean_squared_error
import numpy as np
from sklearn.metrics import roc_auc_score
from kappa import quadratic_weighted_kappa

import hickle
import cPickle

# ================= Metrics ===========================


def MSE(y_true, y_pred, sample_weight=None):
    return mean_squared_error(y_true, y_pred, sample_weight=None)


def RMSE(y_true, y_pred, sample_weight=None):
    return np.sqrt(mean_squared_error(y_true, y_pred, sample_weight=None))


def RMSLE(y_true, y_pred, sample_weight=None):
    return np.sqrt(mean_squared_error(np.log1p(y_true), np.log1p(y_pred),
                                      sample_weight=None))


def AUC(y_true, y_pred, average='macro', sample_weight=None):
    return roc_auc_score(y_true, y_pred, average='macro', sample_weight=None)

def KAPPA(y_true, y_pred, min_rating=None, max_rating=None):
    return quadratic_weighted_kappa(y_true, y_pred, min_rating=min_rating, max_rating=max_rating)

def SOFT_KAPPA(y_true, y_pred, min_rating=None, max_rating=None):
    return 2 * target.dot(y_pred) / (y_true.dot(y_true) + y_pred.dot(y_pred))

# ============= IO ============================

def pdump(data, path, protocol=cPickle.HIGHEST_PROTOCOL):
    cPickle.dump(data, open(path, 'w'), protocol=protocol)


def pload(path):
    return cPickle.load(open(path, 'r'))


def hdump(data, path, compression=None):
    hickle.dump(data, open(path, 'w'), compression=compression)


def hload(path):
    return hickle.load(open(path, 'r'))
